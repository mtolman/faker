#include <faker-rc.h>
#include <doctest/doctest.h>

TEST_SUITE("World Rapid Check") {
    TEST_CASE("Country") {
        CHECK(
                rc::check([](const faker::rc::world::Country &attack) {
                    RC_ASSERT(!attack.name.empty());
                }));
    }
}
