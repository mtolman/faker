#include <faker-rc.h>
#include <doctest/doctest.h>

TEST_SUITE("Malicious Rapid Check") {
    TEST_CASE("SqlInjection") {
        CHECK(
                rc::check([](const faker::rc::malicious::SqlInjectionAttack &attack) {
                    RC_ASSERT(!attack.value.empty());
                }));
    }

    TEST_CASE("Xss") {
        CHECK(
                rc::check([](const faker::rc::malicious::XssAttack &attack) {
                    RC_ASSERT(!attack.value.empty());
                }));
    }

    TEST_CASE("FormatInjection") {
        CHECK(
                rc::check([](const faker::rc::malicious::FormatInjectionAttack &attack) {
                    RC_ASSERT(!attack.value.empty());
                }));
    }

    TEST_CASE("EdgeCase") {
        CHECK(
                rc::check([](const faker::rc::malicious::EdgeCase &attack) {
                }));
    }
}
