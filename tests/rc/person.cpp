#include <faker-rc.h>
#include <rapidcheck.h>
#include <doctest/doctest.h>
#include <regex>

TEST_SUITE("Person Rapid Check") {
    TEST_CASE("Can Pass") {
        SUBCASE("Unrestricted") {
            SUBCASE("Person") {
                CHECK(rc::check([](const faker::person::Person &person) {
                    RC_ASSERT(!person.fullName.empty());
                }));

                CHECK(rc::check([](const faker::rc::person::Person<> &person) {
                    RC_ASSERT(!person->fullName.empty());
                    RC_ASSERT(!person.value.fullName.empty());
                }));
            }

            SUBCASE("Given Name") {
                CHECK(rc::check([](const faker::rc::person::GivenName<> &name) {
                    RC_ASSERT(!name.value.empty());
                }));
            }

            SUBCASE("Surname") {
                CHECK(rc::check([](const faker::rc::person::Surname<> &name) {
                    RC_ASSERT(!name.value.empty());
                }));
            }

            SUBCASE("Full Name") {
                CHECK(rc::check([](const faker::rc::person::FullName<> &name) {
                    RC_ASSERT(!name.value.empty());
                }));
            }
        }

        SUBCASE("Restricted") {
            auto checkRe = std::regex("^[a-zA-Z]+$");

            SUBCASE("Person") {
                CHECK(rc::check([=](const faker::rc::person::Person<faker::DataComplexity::RUDIMENTARY> &person) {
                    RC_ASSERT(std::regex_match(person->givenName, checkRe));
                }));
            }

            SUBCASE("Given Name") {
                CHECK(rc::check([=](const faker::rc::person::GivenName<faker::DataComplexity::RUDIMENTARY> &name) {
                    RC_ASSERT(std::regex_match(name.value, checkRe));
                }));
            }

            SUBCASE("Surname") {
                CHECK(rc::check([=](const faker::rc::person::Surname<faker::DataComplexity::RUDIMENTARY> &name) {
                    RC_ASSERT(std::regex_match(name.value, checkRe));
                }));
            }

            SUBCASE("Full Name") {
                auto checkRe = std::regex("^[a-zA-Z]+ [a-zA-Z]+$");
                CHECK(rc::check([=](const faker::rc::person::FullName<faker::DataComplexity::RUDIMENTARY> &name) {
                    RC_ASSERT(std::regex_match(name.value, checkRe));
                }));
            }

            SUBCASE("Prefix") {
                CHECK(rc::check([=](const faker::rc::person::Prefix<faker::DataComplexity::BASIC> &name) {
                    RC_ASSERT(std::regex_match(name.value.value_or("empty"), checkRe));
                }));
            }

            SUBCASE("Suffix") {
                CHECK(rc::check([=](const faker::rc::person::Suffix<faker::DataComplexity::BASIC> &name) {
                    RC_ASSERT(std::regex_match(name.value.value_or("empty"), checkRe));
                }));
            }
        }
    }

    TEST_CASE("Does Change values to fail") {
        SUBCASE("Changes sex") {
            auto rcRun = rc::check([](const faker::person::Person &person) {
                RC_ASSERT(person.sex != static_cast<faker::person::Sex>(1));
            });
            CHECK_FALSE(rcRun);
        }

        SUBCASE("Changes Name Structure") {
            auto checkRe = std::regex("^[a-zA-Z]+$");
            CHECK_FALSE(rc::check([&](const faker::person::Person &person) {
                RC_ASSERT(std::regex_match(person.givenName, checkRe));
            }));
        }
    }
}
