#include <faker-rc.h>
#include <rapidcheck.h>
#include <doctest/doctest.h>

TEST_SUITE("Color Rapid Check") {
    TEST_CASE("all") {
        CHECK(rc::check([=](const faker::rc::color::RGB &color) {
            RC_ASSERT(!color.css().empty());
        }));

        CHECK(rc::check([=](const faker::rc::color::RGBA &color) {
            RC_ASSERT(!color.css().empty());
        }));

        CHECK(rc::check([=](const faker::rc::color::HSL &color) {
            RC_ASSERT(!color.css().empty());
        }));

        CHECK(rc::check([=](const faker::rc::color::HSLA &color) {
            RC_ASSERT(!color.css().empty());
        }));
    }
}