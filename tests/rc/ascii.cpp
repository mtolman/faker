#include <faker-rc.h>
#include <rapidcheck.h>
#include <doctest/doctest.h>
#include <regex>

TEST_SUITE("Ascii Rapid Check") {
    TEST_CASE("Numeric") {
        std::regex valid("^\\d+$");
        CHECK(rc::check([=](const faker::rc::ascii::Numeric<5> &str) {
            RC_ASSERT(str.value.length() == 5);
            RC_ASSERT(std::regex_match(str.value, valid));
        }));

        CHECK_FALSE(rc::check([](const faker::rc::ascii::Numeric<4> &str) {
            RC_ASSERT(str.value.length() == 5);
        }));
    }

    TEST_CASE("AlphaNumeric") {
        std::regex valid("^[\\da-zA-Z]+$");
        CHECK(rc::check([=](const faker::rc::ascii::AlphaNumeric<5> &str) {
            RC_ASSERT(str.value.length() == 5);
            RC_ASSERT(std::regex_match(str.value, valid));
        }));

        CHECK_FALSE(rc::check([](const faker::rc::ascii::AlphaNumeric<4> &str) {
            RC_ASSERT(str.value.length() == 5);
        }));
    }

    TEST_CASE("Alpha") {
        std::regex valid("^[a-zA-Z]+$");
        CHECK(rc::check([=](const faker::rc::ascii::Alpha<5> &str) {
            RC_ASSERT(str.value.length() == 5);
            RC_ASSERT(std::regex_match(str.value, valid));
        }));

        CHECK_FALSE(rc::check([](const faker::rc::ascii::Alpha<4> &str) {
            RC_ASSERT(str.value.length() == 5);
        }));
    }

    TEST_CASE("Hex") {
        std::regex valid("^[\\da-fA-F]+$");
        CHECK(rc::check([=](const faker::rc::ascii::Hex<5> &str) {
            RC_ASSERT(str.value.length() == 5);
            RC_ASSERT(std::regex_match(str.value, valid));
        }));

        CHECK_FALSE(rc::check([](const faker::rc::ascii::Hex<4> &str) {
            RC_ASSERT(str.value.length() == 5);
        }));
    }
}
