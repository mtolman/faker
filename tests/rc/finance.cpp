#include <faker-rc.h>
#include <doctest/doctest.h>
#include <regex>

TEST_SUITE("Finance Rapid Check") {
    TEST_CASE("Symbol") {
        CHECK_EQ("$", std::string{faker::rc::finance::Sym<faker::finance::CurrencySymbolPos::PREFIX, '$'>::str});
        CHECK_EQ("\u00a3",
                 std::string{faker::rc::finance::Sym<faker::finance::CurrencySymbolPos::PREFIX, '\xc2', '\xa3'>::str});
        CHECK_EQ(faker::finance::CurrencySymbolPos::PREFIX,
                 faker::rc::finance::Sym<faker::finance::CurrencySymbolPos::PREFIX, '\xc2', '\xa3'>::position);
        CHECK_EQ(faker::finance::CurrencySymbolPos::SUFFIX,
                 faker::rc::finance::Sym<faker::finance::CurrencySymbolPos::SUFFIX, '\xc2', '\xa3'>::position);
        CHECK(faker::rc::finance::Sym<faker::finance::CurrencySymbolPos::SUFFIX, '\xc2', '\xa3'>::exists);
        CHECK_FALSE(faker::rc::finance::Sym<>::exists);
        CHECK_EQ("", std::string{faker::rc::finance::Sym<>::str});
    }

    TEST_CASE("AccountNumber") {
        CHECK(
                rc::check([](const faker::rc::finance::AccountNumber<15> &acct) {
                    RC_ASSERT(acct.value.size() == 15);
                }));
    }

    TEST_CASE("AccountName") {
        CHECK(
                rc::check([](const faker::rc::finance::AccountName<faker::DataComplexity::ADVANCED> &acct) {
                    RC_ASSERT(!acct.value.empty());
                }));
    }

    TEST_CASE("TransactionType") {
        CHECK(
                rc::check([](const faker::rc::finance::TransactionType &txt) {
                    RC_ASSERT(txt < faker::rc::finance::TransactionType::COUNT);
                }));
    }

    TEST_CASE("Network") {
        CHECK(
                rc::check([](const faker::rc::finance::Network &txt) {
                    RC_ASSERT(txt < faker::rc::finance::Network::COUNT);
                }));
    }

    TEST_CASE("Amount") {
        CHECK(rc::check([](const faker::rc::finance::Amount<
                faker::rc::finance::AmountRandomCurrency<>
        > &txt) {
            RC_ASSERT(!txt.value.empty());
        }));

        CHECK(rc::check([](const faker::rc::finance::Amount<
                faker::rc::finance::AmountSymOptions<>
        > &txt) {
            RC_ASSERT(!txt.value.empty());
        }));
    }

    TEST_CASE("Bic") {
        CHECK(rc::check([](const faker::rc::finance::Bic<> &txt) {
            RC_ASSERT(!txt.value.empty());
        }));

        CHECK(rc::check([](const faker::rc::finance::Bic<faker::rc::finance::IncludeBranch::SOMETIMES> &txt) {
            RC_ASSERT(!txt.value.empty());
        }));

        CHECK(rc::check([](const faker::rc::finance::Bic<faker::rc::finance::IncludeBranch::ALWAYS> &txt) {
            RC_ASSERT(!txt.value.empty());
        }));

        CHECK(rc::check([](const faker::rc::finance::Bic<faker::rc::finance::IncludeBranch::NEVER> &txt) {
            RC_ASSERT(!txt.value.empty());
        }));
    }

    TEST_CASE("MaskedNumber") {
        CHECK(rc::check([](const faker::rc::finance::MaskedNumber<> &txt) {
            RC_ASSERT(!txt.value.empty());
        }));
    }

    TEST_CASE("Pin") {
        CHECK(rc::check([](const faker::rc::finance::Pin<> &txt) {
            RC_ASSERT(!txt.value.empty());
        }));


        CHECK(rc::check([](const faker::rc::finance::Pin<3, 4> &txt) {
            RC_ASSERT(!txt.value.empty());
        }));
    }

    TEST_CASE("Credit Card") {
        CHECK(rc::check([](const faker::rc::finance::CreditCard &txt) {
            RC_ASSERT(!txt.cvv.empty());
            RC_ASSERT(!txt.expiration.empty());
        }));
    }

    TEST_CASE("Currency") {
        CHECK(rc::check([](const faker::rc::finance::Currency &txt) {
            RC_ASSERT(!txt.name.empty());
        }));
    }
}
