#include <doctest/doctest.h>
#include <faker/finance.h>

#define LUHN_VALIDATION(card) \
    { \
        size_t sum = 0; \
        int multiplier = 2; \
        for (auto chIndex = (card).size() - 1; chIndex > 0; --chIndex) { \
            auto index = chIndex - 1; \
            sum += ((card)[index] - '0') * multiplier; \
            multiplier = multiplier == 1 ? 2 : 1; \
        } \
        const auto check = (9 - ((sum + 9) % 10)); \
        CHECK_EQ(check, ((card)[(card).size() - 1] - '0')); \
    }

auto left_pad(char ch, size_t len, const std::string& str) -> std::string {
    if (str.size() >= len) {
        return str;
    }
    return std::string(len - str.size(), ch) + str;
}

TEST_SUITE("finance") {
    TEST_CASE("Account Name") {
        for (size_t i = 0; i < 1000; ++i) {
            auto res = faker::finance::account_name(std::nullopt, i);
        }
    }

    TEST_CASE("Card Number") {
        for (size_t i = 0; i < 1000; ++i) {
            auto res = faker::finance::credit_card::number(i);
            // Run a Luhn checksum validation to check that we're generating "realistic" cards
            LUHN_VALIDATION(res)
        }
    }

    TEST_CASE("Card") {
        for (size_t i = 0; i < 1000; ++i) {
            auto res = faker::finance::credit_card::card(i);
            // Run a Luhn checksum validation to check that we're generating "realistic" cards
            LUHN_VALIDATION(res.number)

            // Make sure we got a valid CVV
            for (auto ch : res.cvv) {
                CHECK_GE(ch, '0');
                CHECK_LE(ch, '9');
            }

            auto month = std::stoi(res.expiration.substr(0, 2));
            CHECK_GE(month, 1);
            CHECK_LE(month, 12);

            auto year = std::stoi(res.expiration.substr(3));
            CHECK_GE(year, 1900);
        }
    }

    TEST_CASE("Account Number") {
        for (size_t i = 0; i < 1000; ++i) {
            auto res = faker::finance::account_number(12, i);
            auto parsed = std::stoll(res, nullptr, 10);
            CHECK_GT(parsed, 0);
            CHECK_EQ(left_pad('0', 12, std::to_string(parsed)), res);
        }
    }

    TEST_CASE("Transaction Type") {
        for (size_t i = 0; i < 8; ++i) {
            auto res = faker::finance::transaction_type(i);
            CHECK_LT(res, faker::finance::TransactionType::COUNT);
            CHECK_GE(res, static_cast<faker::finance::TransactionType>(0));
        }
    }

    TEST_CASE("Amount") {
        SUBCASE("default") {
            constexpr auto max = 1000000;
            constexpr auto min = 0;
            for (size_t i = 0; i < 1000; ++i) {
                auto res = faker::finance::amount({.max = max, .min = min}, i);
                auto parsed = std::stod(res);
                CHECK_LE(parsed, max);
                CHECK_GE(parsed, 0);
                CHECK_EQ(res[res.size() - 3], '.');
            }
        }

        SUBCASE("more decimals") {
            constexpr auto max = 1000000;
            constexpr auto min = 0;
            for (size_t i = 0; i < 1000; ++i) {
                auto res = faker::finance::amount({.decimalPlaces = 4, .max = max, .min = min}, i);
                auto parsed = std::stod(res);
                CHECK_LE(parsed, max);
                CHECK_GE(parsed, min);
                CHECK_EQ(res[res.size() - 5], '.');
            }
        }

        SUBCASE("no decimals") {
            constexpr auto max = 1000000;
            constexpr auto min = 0;
            for (size_t i = 0; i < 1000; ++i) {
                auto res = faker::finance::amount({.decimalPlaces = 0, .max = max, .min = min}, i);
                auto parsed = std::stod(res);
                CHECK_LE(parsed, max);
                CHECK_GE(parsed, min);
                CHECK_EQ(res.find_first_of('.'), std::string::npos);
            }
        }

        SUBCASE("different decimal separator") {
            constexpr auto max = 1000000;
            constexpr auto min = 0;
            for (size_t i = 0; i < 1000; ++i) {
                auto res = faker::finance::amount({.max = max, .min = min, .decimalSeparator = ","}, i);
                auto parsed = std::stod(res);
                CHECK_LE(parsed, max);
                CHECK_GE(parsed, min);
                CHECK_EQ(res[res.size() - 3], ',');
                CHECK_EQ(res.find_first_of('.'), std::string::npos);
            }
        }

        SUBCASE("Prefix Symbol") {
            constexpr auto max = 1000000;
            constexpr auto min = 0;
            for (size_t i = 0; i < 1000; ++i) {
                auto res = faker::finance::amount(
                        {
                            .max = max,
                            .min = min,
                            .symbol = "$",
                            .symbolPos=faker::finance::CurrencySymbolPos::PREFIX
                        },
                        i
                );
                CHECK_EQ(res[0], '$');
                res = res.substr(1);

                auto parsed = std::stod(res);
                CHECK_LE(parsed, max);
                CHECK_GE(parsed, min);
                CHECK_EQ(res[res.size() - 3], '.');
            }
        }

        SUBCASE("Suffix Symbol") {
            constexpr auto max = 100000;
            constexpr auto min = 0;
            for (size_t i = 0; i < 1000; ++i) {
                auto res = faker::finance::amount({
                        .max = max,
                        .min = min,
                        .symbol = "$US",
                        .symbolPos=faker::finance::CurrencySymbolPos::SUFFIX
                    },i);
                CHECK_EQ(res.substr(res.size() - 3), "$US");
                res = res.substr(0, res.size() - 3);

                auto parsed = std::stod(res);
                CHECK_LE(parsed, max);
                CHECK_GE(parsed, min);
                CHECK_EQ(res[res.size() - 3], '.');
            }
        }
    }
}
