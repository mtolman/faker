#pragma once

#include <doctest/doctest.h>

#define CHECK_RANGE(actual, expected, delta) CHECK_GE(actual, expected - delta); CHECK_LE(actual, expected + delta);
