#include <faker/world.h>
#include <doctest/doctest.h>

TEST_SUITE("World") {
    TEST_CASE("Country") {
        using namespace faker::world;
        for (auto i = 0; i < 1000; ++i) {
            auto c = country(i);
            auto alpha2 = country_code({CountryCodeType::ALPHA2}, i);
            auto alpha3 = country_code({CountryCodeType::ALPHA3}, i);
            auto numeric = country_code({CountryCodeType::NUMERIC}, i);
            auto name = country_name(i);
            auto domain = country_domain(i);

            CHECK_EQ(c.name, name);
            CHECK_EQ(c.alpha2, alpha2);
            CHECK_EQ(c.alpha3, alpha3);
            CHECK_EQ(c.numericCode, numeric);
            CHECK_EQ(c.domain, domain);
        }
    }
}
