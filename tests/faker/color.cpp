#include <faker/color.h>
#include <doctest/doctest.h>
#include "helpers.h"

TEST_SUITE("Color") {
    TEST_CASE("RGB") {
        CHECK_EQ(faker::color::rgb(1).css(), "rgb(34, 35, 115)");
        CHECK_EQ(faker::color::rgb(19).css(), "rgb(187, 141, 38)");

        auto rgb = faker::color::rgb(1);
        CHECK_EQ(rgb.bytes()[0], 34);
        CHECK_EQ(rgb.bytes()[1], 35);
        CHECK_EQ(rgb.bytes()[2], 115);

        CHECK_RANGE(rgb.red, 0.133877, 0.00001);
        CHECK_RANGE(rgb.green, 0.136407, 0.00001);
        CHECK_RANGE(rgb.blue, 0.451215, 0.00001);

        CHECK_EQ(faker::color::rgb(1).hex(""), "222373");
        CHECK_EQ(faker::color::rgb(19).hex("#"), "#BB8D26");
    }

    TEST_CASE("RGBA") {
        CHECK_EQ(faker::color::rgba(1).css(), "rgba(34, 35, 115, 5)");
        CHECK_EQ(faker::color::rgba(19).css(), "rgba(187, 141, 38, 13)");

        auto rgba = faker::color::rgba(1);
        CHECK_EQ(rgba.bytes()[0], 34);
        CHECK_EQ(rgba.bytes()[1], 35);
        CHECK_EQ(rgba.bytes()[2], 115);
        CHECK_EQ(rgba.bytes()[3], 5);

        CHECK_RANGE(rgba.red, 0.133877, 0.00001);
        CHECK_RANGE(rgba.green, 0.136407, 0.00001);
        CHECK_RANGE(rgba.blue, 0.451215, 0.00001);
        CHECK_RANGE(rgba.alpha, 0.0210242, 0.00001);

        CHECK_EQ(faker::color::rgba(1).hex(""), "22237305");
        CHECK_EQ(faker::color::rgba(19).hex("#"), "#BB8D260D");
    }

    TEST_CASE("HSL") {
        CHECK_EQ(faker::color::hsl(1).css(), "hsl(48, 14%, 45%)");
        CHECK_EQ(faker::color::hsl(19).css(), "hsl(264, 55%, 15%)");

        auto hsl = faker::color::hsl(1);
        CHECK_RANGE(hsl.hue, 48.1956, 0.00001);
        CHECK_RANGE(hsl.saturation, 0.136407, 0.00001);
        CHECK_RANGE(hsl.luminosity, 0.451215, 0.00001);
    }

    TEST_CASE("HSLA") {
        CHECK_EQ(faker::color::hsla(1).css(), "hsla(48, 14%, 45%, 0.0210242)");
        CHECK_EQ(faker::color::hsla(19).css(), "hsla(264, 55%, 15%, 0.0497917)");

        auto hsla = faker::color::hsla(1);
        CHECK_RANGE(hsla.hue, 48.1956, 0.00001);
        CHECK_RANGE(hsla.saturation, 0.136407, 0.00001);
        CHECK_RANGE(hsla.luminosity, 0.451215, 0.00001);
        CHECK_RANGE(hsla.alpha, 0.0210242, 0.0001);
    }
}
