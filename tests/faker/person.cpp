#include <doctest/doctest.h>
#include <faker/person.h>
#include <regex>

static const auto iterations = 5000;

TEST_SUITE("Person") {
    TEST_CASE("Prefix") {
        for (auto i = 0; i < 50; ++i) {
            REQUIRE_FALSE(faker::person::prefix(faker::DataComplexity::RUDIMENTARY).has_value());
            auto basic = faker::person::prefix(faker::DataComplexity::BASIC);
            if (basic.has_value()) {
                REQUIRE(!std::string{*basic}.empty());
            }

            auto advanced = faker::person::prefix(faker::DataComplexity::ADVANCED);
            if (advanced.has_value()) {
                REQUIRE(!std::string{*advanced}.empty());
            }

            auto intermediate = faker::person::prefix(faker::DataComplexity::INTERMEDIATE);
            if (intermediate.has_value()) {
                REQUIRE(!std::string{*intermediate}.empty());
            }

            auto complex = faker::person::prefix(faker::DataComplexity::COMPLEX);
            if (complex.has_value()) {
                REQUIRE(!std::string{*complex}.empty());
            }
        }
    }

    TEST_CASE("Suffix") {
        for (auto i = 0; i < 50; ++i) {
            REQUIRE_FALSE(faker::person::suffix(faker::DataComplexity::RUDIMENTARY).has_value());
            auto basic = faker::person::suffix(faker::DataComplexity::BASIC);
            if (basic.has_value()) {
                REQUIRE(!std::string{*basic}.empty());
            }

            auto advanced = faker::person::suffix(faker::DataComplexity::ADVANCED);
            if (advanced.has_value()) {
                REQUIRE(!std::string{*advanced}.empty());
            }

            auto intermediate = faker::person::suffix(faker::DataComplexity::INTERMEDIATE);
            if (intermediate.has_value()) {
                REQUIRE(!std::string{*intermediate}.empty());
            }

            auto complex = faker::person::suffix(faker::DataComplexity::COMPLEX);
            if (complex.has_value()) {
                REQUIRE(!std::string{*complex}.empty());
            }
        }
    }

    TEST_CASE("Given Name") {
        SUBCASE("Rudimentary") {
            auto matchRegex = std::regex("^[a-zA-Z]+$");
            for (auto i = 0; i < iterations; ++i) {
                auto name = faker::person::given_name(faker::DataComplexity::RUDIMENTARY, std::nullopt, i);
                REQUIRE(std::regex_match(name, matchRegex));
            }
        }

        SUBCASE("Intermediate") {
            auto matchRegex = std::regex("^[\\x20-\\x7E]+$");
            for (auto i = 0; i < iterations; ++i) {
                auto name = faker::person::given_name(faker::DataComplexity::INTERMEDIATE, std::nullopt, i);
                REQUIRE(std::regex_match(name, matchRegex));
            }
        }

        SUBCASE("Basic") {
            auto matchRegex = std::regex("^[\\x20-\\x7E]+$");
            for (auto i = 0; i < iterations; ++i) {
                auto name = faker::person::given_name(faker::DataComplexity::BASIC, std::nullopt, i);
                REQUIRE(std::regex_match(name, matchRegex));
            }
        }

        SUBCASE("Advanced") {
            auto matchRegex = std::regex(".*[a-zA-Z].*");
            for (auto i = 0; i < iterations; ++i) {
                auto name = faker::person::given_name(faker::DataComplexity::ADVANCED, std::nullopt, i);
                REQUIRE(std::regex_match(name, matchRegex));
            }
        }

        SUBCASE("Complex") {
            auto matchRegex = std::regex(".*[a-zA-Z].*");
            for (auto i = 0; i < iterations; ++i) {
                auto name = faker::person::given_name(faker::DataComplexity::COMPLEX, std::nullopt, i);
                REQUIRE(!std::regex_match(name, matchRegex));
            }
        }
    }

    TEST_CASE("Surname") {
        SUBCASE("Rudimentary") {
            auto matchRegex = std::regex("^[a-zA-Z]+$");
            for (auto i = 0; i < iterations; ++i) {
                auto name = faker::person::surname(faker::DataComplexity::RUDIMENTARY, i);
                REQUIRE(std::regex_match(name, matchRegex));
            }
        }

        SUBCASE("Intermediate") {
            auto matchRegex = std::regex("^[\\x20-\\x7E]+$");
            for (auto i = 0; i < iterations; ++i) {
                auto name = faker::person::surname(faker::DataComplexity::INTERMEDIATE, i);
                REQUIRE(std::regex_match(name, matchRegex));
            }
        }

        SUBCASE("Basic") {
            auto matchRegex = std::regex("^[\\x20-\\x7E]+$");
            for (auto i = 0; i < iterations; ++i) {
                auto name = faker::person::surname(faker::DataComplexity::BASIC, i);
                REQUIRE(std::regex_match(name, matchRegex));
            }
        }

        SUBCASE("Advanced") {
            auto matchRegex = std::regex(".*[a-zA-Z].*");
            for (auto i = 0; i < iterations; ++i) {
                auto name = faker::person::surname(faker::DataComplexity::ADVANCED, i);
                REQUIRE(std::regex_match(name, matchRegex));
            }
        }

        SUBCASE("Complex") {
            auto matchRegex = std::regex(".*[a-zA-Z].*");
            for (auto i = 0; i < iterations; ++i) {
                auto name = faker::person::surname(faker::DataComplexity::COMPLEX, i);
                REQUIRE(!std::regex_match(name, matchRegex));
            }
        }
    }

    TEST_CASE("Full Name") {
        SUBCASE("Rudimentary") {
            auto matchRegex = std::regex("^[a-zA-Z]+ [a-zA-Z]+$");
            for (auto i = 0; i < iterations; ++i) {
                auto name = faker::person::full_name(faker::DataComplexity::RUDIMENTARY, std::nullopt, i);
                REQUIRE(std::regex_match(name, matchRegex));
            }
        }

        SUBCASE("Basic") {
            auto matchRegex = std::regex(R"(^[\x20-\x7E]+ [\x20-\x7E]+$)");
            for (auto i = 0; i < iterations; ++i) {
                auto name = faker::person::full_name(faker::DataComplexity::BASIC, std::nullopt, i);
                REQUIRE(std::regex_match(name, matchRegex));
            }
        }

        SUBCASE("Advanced") {
            auto matchRegex = std::regex(".*[a-zA-Z].* .*[a-zA-Z].*");
            for (auto i = 0; i < iterations; ++i) {
                auto name = faker::person::full_name(faker::DataComplexity::ADVANCED, std::nullopt, i);
                REQUIRE(std::regex_match(name, matchRegex));
            }
        }

        SUBCASE("Complex") {
            auto matchRegex = std::regex(".*[a-zA-Z].*");
            for (auto i = 0; i < iterations; ++i) {
                auto name = faker::person::full_name(faker::DataComplexity::COMPLEX, std::nullopt, i);
                REQUIRE(!std::regex_match(name, matchRegex));
            }
        }
    }

    TEST_CASE("Person") {
        SUBCASE("Rudimentary") {
            auto fullRegex = std::regex("^[a-zA-Z]+ [a-zA-Z]+$");
            auto partRegex = std::regex("^[a-zA-Z]+$");
            for (auto i = 0; i < iterations; ++i) {
                auto person = faker::person::person(faker::DataComplexity::RUDIMENTARY, std::nullopt, i);
                REQUIRE(std::regex_match(person.fullName, fullRegex));
                REQUIRE(std::regex_match(person.givenName, partRegex));
                REQUIRE(std::regex_match(person.surName, partRegex));
                REQUIRE_LT(person.sex, faker::person::Sex::COUNT);
                REQUIRE(person.fullName.find(person.givenName) != std::string::npos);
                REQUIRE(person.fullName.find(person.surName) != std::string::npos);
            }
        }

        SUBCASE("Basic") {
            auto fullRegex = std::regex(R"(^[\x20-\x7E]+ [\x20-\x7E]+$)");
            auto partRegex = std::regex("^[\\x20-\\x7E]+$");
            for (auto i = 0; i < iterations; ++i) {
                auto person = faker::person::person(faker::DataComplexity::BASIC, std::nullopt, i);
                REQUIRE(std::regex_match(person.fullName, fullRegex));
                REQUIRE(std::regex_match(person.givenName, partRegex));
                REQUIRE(std::regex_match(person.surName, partRegex));
                REQUIRE_LT(person.sex, faker::person::Sex::COUNT);
                REQUIRE(person.fullName.find(person.givenName) != std::string::npos);
                REQUIRE(person.fullName.find(person.surName) != std::string::npos);
            }
        }

        SUBCASE("Advanced") {
            auto fullRegex = std::regex(R"(.*[a-zA-Z].* .*[a-zA-Z].*)");
            auto partRegex = std::regex("^.*[a-zA-Z].*$");
            for (auto i = 0; i < iterations; ++i) {
                auto person = faker::person::person(faker::DataComplexity::ADVANCED, std::nullopt, i);
                REQUIRE(std::regex_match(person.fullName, fullRegex));
                REQUIRE(std::regex_match(person.givenName, partRegex));
                REQUIRE(std::regex_match(person.surName, partRegex));
                REQUIRE_LT(person.sex, faker::person::Sex::COUNT);
                REQUIRE(person.fullName.find(person.givenName) != std::string::npos);
                REQUIRE(person.fullName.find(person.surName) != std::string::npos);
            }
        }

        SUBCASE("Complex") {
            auto matchRegex = std::regex(".*[a-zA-Z].*");
            for (auto i = 0; i < iterations; ++i) {
                auto person = faker::person::person(faker::DataComplexity::COMPLEX, std::nullopt, i);
                REQUIRE(!std::regex_match(person.fullName, matchRegex));
                REQUIRE(!std::regex_match(person.givenName, matchRegex));
                REQUIRE(!std::regex_match(person.surName, matchRegex));
                REQUIRE_LT(person.sex, faker::person::Sex::COUNT);
                REQUIRE(person.fullName.find(person.givenName) != std::string::npos);
                REQUIRE(person.fullName.find(person.surName) != std::string::npos);
            }
        }

        SUBCASE("Malicious") {
            for (auto i = 0; i < iterations; ++i) {
                auto person = faker::person::person(faker::DataComplexity::COMPLEX, std::nullopt, i);
                REQUIRE_FALSE(std::string{person.fullName}.empty());
                REQUIRE_FALSE(std::string{person.givenName}.empty());
                REQUIRE_FALSE(std::string{person.surName}.empty());
                REQUIRE_LT(person.sex, faker::person::Sex::COUNT);
                REQUIRE(person.fullName.find(person.givenName) != std::string::npos);
            }
        }

        SUBCASE("Any") {
            for (auto i = 0; i < iterations; ++i) {
                auto person = faker::person::person(std::nullopt, std::nullopt, i);
                REQUIRE_LT(person.sex, faker::person::Sex::COUNT);
                REQUIRE(person.fullName.find(person.givenName) != std::string::npos);
            }
        }
    }
}
