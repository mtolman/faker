#pragma once

#include <random>
#include <faker/ascii.h>
#include <faker/common.h>
#include "../optionals.h"
#include "random.h"

namespace faker::generators {
    auto numeric_str(const faker::ascii::NumericOpts& opts, std::mt19937_64& rnd) -> std::string;
    auto alpha_str(const faker::ascii::AlphaOpts& opts, std::mt19937_64& rnd) -> std::string;
    auto alpha_num_str(const faker::ascii::AlphaNumOpts& opts, std::mt19937_64& rnd) -> std::string;
}

namespace faker::utils {
    inline auto init_index(std::optional<size_t> index) -> size_t {
        return faker::opts::or_else(index, [](){ return faker::random::Random::integer<size_t>(); });
    }

    inline auto init_rand(std::optional<size_t> index) -> std::tuple<size_t, std::mt19937_64> {
        auto s = init_index(index);
        auto r = std::mt19937_64(s);
        return {
                s,
                r
        };
    }

    inline auto init_complexity(std::optional<faker::DataComplexity> dataComplexity, std::optional<size_t> index)
    -> std::tuple<faker::DataComplexity, size_t> {
        auto dataIndex = init_index(index);

        auto complexity = std::min(faker::opts::or_else(dataComplexity, [&dataIndex](){
            auto res = static_cast<faker::DataComplexity>(dataIndex % static_cast<size_t>(faker::DataComplexity::COUNT));
            dataIndex /= static_cast<size_t>(faker::DataComplexity::COUNT);
            return res;
        }), static_cast<faker::DataComplexity>(static_cast<int>(faker::DataComplexity::COUNT) - 1));

        return {complexity, dataIndex};
    }
}
