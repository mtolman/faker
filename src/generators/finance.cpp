#include <faker/finance.h>
#include <faker/world.h>
#include "../data/finance.h"
#include "../data/currency.h"
#include "../optionals.h"
#include "random.h"
#include <tuple>
#include "common.h"
#include <cstdio>
#include <regex>
#include <sstream>
#include <array>

auto faker::finance::account_number(size_t length, std::optional<size_t> index) -> std::string {
    auto [seed, rnd] = faker::utils::init_rand(index);
    return faker::generators::numeric_str({.length = length}, rnd);
}

auto faker::finance::amount(const faker::finance::AmountOptions& options, std::optional<size_t> index) -> std::string {
    auto [seed, rnd] = faker::utils::init_rand(index);
    auto val = std::uniform_real_distribution<double>(options.min, options.max)(rnd);
    const auto fmtStr = "%." + std::to_string(options.decimalPlaces) + "f";
    char* buffer = nullptr;
    asprintf(&buffer, fmtStr.c_str(), val);
    auto res = std::string{buffer};
    free(buffer);

    if (options.decimalPlaces && options.decimalSeparator != ".") {
        res = std::regex_replace(res, std::regex("\\."), options.decimalSeparator);
    }

    if (!options.symbol.empty()) {
        if (options.symbolPos == CurrencySymbolPos::PREFIX) {
            res = options.symbol + res;
        }
        else {
            res += options.symbol;
        }
    }

    return res;
}

auto faker::finance::account_name(std::optional<faker::DataComplexity> complexity,
                                  std::optional<size_t> index) -> std::string {
    auto [c, i] = faker::utils::init_complexity(complexity, index);
    return faker::data::account_name(c, i);
}

auto faker::finance::transaction_type(std::optional<size_t> index) -> TransactionType {
    auto i = faker::utils::init_index(index);
    return static_cast<TransactionType>(i % static_cast<int>(TransactionType::COUNT));
}

auto faker::finance::bic(faker::finance::BicOptions options, std::optional<size_t> index) -> std::string {
    auto [s, rand] = faker::utils::init_rand(index);
    std::stringstream ss;
    // institution/bank code
    ss << faker::generators::alpha_str({.length = 4, .casing = ascii::Casing::UPPER}, rand);

    // country code
    ss << faker::world::country_code({.type = world::CountryCodeType::ALPHA2}, s);

    // location code
    ss << faker::generators::alpha_num_str({.length = 2, .casing = ascii::Casing::UPPER}, rand);

    if (options.includeBranchCode) {
        ss << faker::generators::alpha_num_str({.length = 3, .casing = ascii::Casing::UPPER}, rand);
    }

    return ss.str();
}

auto faker::finance::masked_number(const faker::finance::MaskedNumberOptions &options,
                                   std::optional<size_t> index) -> std::string {
    auto [_, rand] = faker::utils::init_rand(index);
    auto res = std::string(options.maskedLength, options.maskedChar) + faker::generators::numeric_str({.length = options.unMaskedLength}, rand);
    if (options.surroundWithParens) {
        return "(" + res + ")";
    }
    return res;
}

auto faker::finance::pin(faker::finance::PinOptions options, std::optional<size_t> index) -> std::string {
    auto [_, rand] = faker::utils::init_rand(index);
    return faker::generators::numeric_str({.length = options.length}, rand);
}

auto faker::finance::currency(std::optional<size_t> index) -> faker::finance::Currency {
    const auto i = faker::utils::init_index(index);
    return faker::data::currency(i);
}

static auto cvv_impl(faker::finance::credit_card::Network network, std::mt19937_64& rnd) -> std::string {
    switch (network) {
        case faker::finance::credit_card::Network::AMERICAN_EXPRESS:
            return faker::generators::numeric_str({.length = 4}, rnd);
        default:
            return faker::generators::numeric_str({.length = 3}, rnd);
    }
}

static auto card_check_digit(const std::string& cardNumber) -> char {
    size_t sum = 0;
    int multiplier = 2;
    for (auto i = cardNumber.size(); i > 0; --i) {
        const auto index = i - 1;
        sum += (cardNumber[index] - '0') * multiplier;
        // flip between 1 and 2
        multiplier ^= 3;
    }
    return static_cast<char>(9 - (sum + 9) % 10) + '0';
}

static auto generate_mastercard(std::mt19937_64& rnd) -> std::string {
    static const auto starts = std::array{"51", "52", "53", "54", "55"};
    static const auto len = 16;
    auto start = std::string{starts.at(std::uniform_int_distribution(0, 1)(rnd))};
    auto res = start + faker::generators::numeric_str({.length = len - 1 - start.size()}, rnd);
    return res + card_check_digit(res);
}

static auto generate_visa(std::mt19937_64& rnd) -> std::string  {
    static const auto starts = std::array{"4539", "4556", "4916", "4532", "4929", "40240071", "4485", "4716", "4"};
    auto len = (std::uniform_int_distribution(0, 1)(rnd) == 0) ? 13 : 16;

    auto start = std::string{starts.at(std::uniform_int_distribution(0, 1)(rnd))};
    auto res = start + faker::generators::numeric_str({.length = len - 1 - start.size()}, rnd);
    return res + card_check_digit(res);
}

static auto generate_american_express(std::mt19937_64 &rnd) -> std::string {
    static const auto starts = std::array{"34", "37"};
    static const auto len = 15;
    auto start = std::string{starts.at(std::uniform_int_distribution(0, 1)(rnd))};
    auto res = start + faker::generators::numeric_str({.length = len - 1 - start.size()}, rnd);
    return res + card_check_digit(res);
}

static auto generate_discover(std::mt19937_64& rnd) -> std::string {
    static const auto len = 16;
    auto start = std::string{"6011"};
    auto res = start + faker::generators::numeric_str({.length = len - 1 - start.size()}, rnd);
    return res + card_check_digit(res);
}

static auto generate_diners(std::mt19937_64& rnd) -> std::string {
    static const auto starts = std::array{"300", "301", "302", "303", "36", "38"};
    static const auto len = 15;

    auto start = std::string{starts.at(std::uniform_int_distribution(0, 1)(rnd))};
    auto res = start + faker::generators::numeric_str({.length = len - 1 - start.size()}, rnd);
    return res + card_check_digit(res);
}

static auto generate_voyager(std::mt19937_64& rnd) -> std::string {
    auto len = (std::uniform_int_distribution(0, 1)(rnd) == 0) ? 13 : 16;

    auto start = std::string{"8699"};
    auto res = start + faker::generators::numeric_str({.length = len - 1 - start.size()}, rnd);
    return res + card_check_digit(res);
}

static auto generate_enroute(std::mt19937_64& rnd) -> std::string {
    static const auto starts = std::array{"2014", "2149"};
    static const auto len = 16;
    auto start = std::string{starts.at(std::uniform_int_distribution(0, 1)(rnd))};
    auto res = start + faker::generators::numeric_str({.length = len - 1 - start.size()}, rnd);
    return res + card_check_digit(res);
}

static auto generate_jcb_15(std::mt19937_64& rnd) -> std::string {
    static const auto starts = std::array{"2100", "1800"};
    static const auto len = 16;
    auto start = std::string{starts.at(std::uniform_int_distribution(0, 1)(rnd))};
    auto res = start + faker::generators::numeric_str({.length = len - 1 - start.size()}, rnd);
    return res + card_check_digit(res);
}

static auto generate_jcb_16(std::mt19937_64& rnd) -> std::string {
    static const auto starts = std::array{"3088", "3096", "3112", "3158", "3337", "3528"};
    static const auto len = 16;
    auto start = std::string{starts.at(std::uniform_int_distribution(0, 1)(rnd))};
    auto res = start + faker::generators::numeric_str({.length = len - 1 - start.size()}, rnd);
    return res + card_check_digit(res);
}

static auto card_num_impl(faker::finance::credit_card::Network network, std::mt19937_64& rnd) -> std::string {
    switch (network) {
        case faker::finance::credit_card::Network::MASTERCARD:
            return generate_mastercard(rnd);
        case faker::finance::credit_card::Network::VISA:
            return generate_visa(rnd);
        case faker::finance::credit_card::Network::AMERICAN_EXPRESS:
            return generate_american_express(rnd);
        case faker::finance::credit_card::Network::DISCOVER:
            return generate_discover(rnd);
        case faker::finance::credit_card::Network::DINERS:
            return generate_diners(rnd);
        case faker::finance::credit_card::Network::VOYAGER:
            return generate_voyager(rnd);
        case faker::finance::credit_card::Network::ENROUTE:
            return generate_enroute(rnd);
        case faker::finance::credit_card::Network::JCB15:
            return generate_jcb_15(rnd);
        case faker::finance::credit_card::Network::COUNT:
        case faker::finance::credit_card::Network::JCB16:
            return generate_jcb_16(rnd);
    }
    return "";
}

static auto expiration_impl(std::mt19937_64& rnd) -> std::string {
    std::stringstream ss;
    auto month = std::uniform_int_distribution(1, 12)(rnd);
    auto year = std::uniform_int_distribution(2000, 2400)(rnd);
    constexpr auto lpad = [](std::stringstream& ss, int i) {
        if (i < 10) {
            ss << '0';
        }
        ss << i;
    };
    lpad(ss, month);
    ss << "/" << year;
    return ss.str();
}

auto faker::finance::credit_card::card(std::optional<size_t> index) -> faker::finance::credit_card::Card {
    auto [i, rnd] = faker::utils::init_rand(index);
    auto cardNetwork = network(i);
    return {
        .cvv = cvv_impl(cardNetwork, rnd),
        .network = cardNetwork,
        .number = card_num_impl(cardNetwork, rnd),
        .expiration = expiration(i),
    };
}

auto faker::finance::credit_card::cvv(std::optional<size_t> index) -> std::string {
    auto [i, rnd] = faker::utils::init_rand(index);
    return cvv_impl(network(i), rnd);
}

auto faker::finance::credit_card::expiration(std::optional<size_t> index) -> std::string {
    auto [i, rnd] = faker::utils::init_rand(index);
    return expiration_impl(rnd);
}

auto faker::finance::credit_card::number(std::optional<size_t> index) -> std::string {
    auto [i, rnd] = faker::utils::init_rand(index);
    return card_num_impl(network(i), rnd);
}

auto faker::finance::credit_card::network(std::optional<size_t> index) -> faker::finance::credit_card::Network {
    auto i = faker::utils::init_index(index);
    return static_cast<faker::finance::credit_card::Network>(i % static_cast<decltype(i)>(faker::finance::credit_card::Network::COUNT));
}
