#include <faker/ascii.h>
#include "common.h"

auto faker::ascii::numeric(const faker::ascii::NumericOpts& options, std::optional<size_t> index) -> std::string {
    auto [_, rand] = faker::utils::init_rand(index);
    return faker::generators::numeric_str(options, rand);
}

auto faker::ascii::alpha(const faker::ascii::AlphaOpts& opts, std::optional<size_t> index) -> std::string {
    auto [_, rand] = faker::utils::init_rand(index);
    return faker::generators::alpha_str(opts, rand);
}

auto faker::ascii::alpha_num(const faker::ascii::AlphaNumOpts &opts, std::optional<size_t> index) -> std::string {
    auto [_, rand] = faker::utils::init_rand(index);
    return faker::generators::alpha_num_str(opts, rand);
}

