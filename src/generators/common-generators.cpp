#include "common.h"
#include <sstream>
#include <array>

static auto numeric_make_possibilities(const std::set<char>& exclude) -> std::vector<char> {
    auto fullSet = std::set{'0', '1', '2', '3', '4', '5', '6', '7', '8', '9'};
    for (const auto& ch : exclude) {
        fullSet.erase(ch);
    }
    if (fullSet.empty()) {
        return {};
    }

    auto chars = std::vector<char>{};
    chars.resize(fullSet.size());
    std::copy(fullSet.begin(), fullSet.end(), chars.begin());
    return chars;
}

static auto numeric_char(const std::optional<std::vector<char>> &possibilities, std::mt19937_64 &rnd) -> char {
    if (!possibilities || possibilities->empty()) {
        return std::uniform_int_distribution('0', '9')(rnd);
    }

    return possibilities->at(std::uniform_int_distribution<size_t>(0, possibilities->size() - 1)(rnd));
}

static const auto alphaChars = std::array{
    'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o',
    'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'A', 'B', 'C', 'D',
    'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S',
    'T', 'U', 'V', 'W', 'X', 'Y', 'Z',
};

static auto alpha_make_possibilities(const std::set<char>& exclude) -> std::vector<char> {
    auto fullSet = std::vector<char>{};
    for (const auto& c : alphaChars) {
        if (exclude.find(c) == exclude.end()) {
            fullSet.push_back(c);
        }
    }
    return fullSet;
}

static auto alpha_char(const std::optional<std::vector<char>> &possibilities, std::mt19937_64 &rnd) -> char {
    if (!possibilities || possibilities->empty()) {
        return alphaChars[std::uniform_int_distribution<size_t>(0, alphaChars.size() - 1)(rnd)];
    }

    return possibilities->at(std::uniform_int_distribution<size_t>(0, possibilities->size() - 1)(rnd));
}

static const auto alphaNumChars = std::array{
        'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o',
        'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'A', 'B', 'C', 'D',
        'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S',
        'T', 'U', 'V', 'W', 'X', 'Y', 'Z', '0', '1', '2', '3', '4', '5', '6', '7',
        '8', '9', '0'
};

static auto alpha_num_make_possibilities(const std::set<char>& exclude) -> std::vector<char> {
    auto fullSet = std::vector<char>{};
    for (const auto& c : alphaNumChars) {
        if (exclude.find(c) == exclude.end()) {
            fullSet.push_back(c);
        }
    }
    return fullSet;
}

static auto alpha_num_char(const std::optional<std::vector<char>> &possibilities, std::mt19937_64 &rnd) -> char {
    if (!possibilities || possibilities->empty()) {
        return alphaNumChars[std::uniform_int_distribution<size_t>(0, alphaNumChars.size() - 1)(rnd)];
    }

    return possibilities->at(std::uniform_int_distribution<size_t>(0, possibilities->size() - 1)(rnd));
}

auto faker::generators::numeric_str(const faker::ascii::NumericOpts &opts, std::mt19937_64 &rnd) -> std::string {
    if (opts.length == 0) {
        return "";
    }

    std::stringstream ss;
    auto len = opts.length;
    if (!opts.allowLeadingZeros) {
        auto withZeroExcluded = opts.exclude.value_or(std::set<char>{});
        withZeroExcluded.insert('0');
        ss << numeric_char(numeric_make_possibilities(withZeroExcluded), rnd);
        --len;
    }

    std::optional<std::vector<char>> possible = std::nullopt;
    if (opts.exclude.has_value()) {
        possible = numeric_make_possibilities(*opts.exclude);
    }

    while (len-- > 0) {
        ss << numeric_char(possible, rnd);
    }
    return ss.str();
}

auto faker::generators::alpha_str(const faker::ascii::AlphaOpts &opts, std::mt19937_64 &rnd) -> std::string {
    if (opts.length == 0) {
        return "";
    }

    auto exclusions = opts.exclude.value_or(std::set<char>{});
    if (opts.casing == ascii::Casing::UPPER) {
        exclusions.merge(std::set<char>{'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z'});
    }

    if (opts.casing == ascii::Casing::LOWER) {
        exclusions.merge(std::set<char>{'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'});
    }

    std::stringstream ss;
    auto len = opts.length;

    std::optional<std::vector<char>> possible = std::nullopt;
    if (!exclusions.empty()) {
        possible = alpha_make_possibilities(exclusions);
    }

    while (len-- > 0) {
        ss << alpha_char(possible, rnd);
    }
    return ss.str();
}

auto faker::generators::alpha_num_str(const faker::ascii::AlphaNumOpts &opts, std::mt19937_64 &rnd) -> std::string {
    if (opts.length == 0) {
        return "";
    }

    auto exclusions = opts.exclude.value_or(std::set<char>{});
    if (opts.casing == ascii::Casing::UPPER) {
        exclusions.merge(std::set<char>{'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z'});
    }

    if (opts.casing == ascii::Casing::LOWER) {
        exclusions.merge(std::set<char>{'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'});
    }

    std::stringstream ss;
    auto len = opts.length;

    std::optional<std::vector<char>> possible = std::nullopt;
    if (!exclusions.empty()) {
        possible = alpha_num_make_possibilities(exclusions);
    }

    while (len-- > 0) {
        ss << alpha_num_char(possible, rnd);
    }
    return ss.str();
}