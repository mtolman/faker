#include "faker/person.h"
#include "../data/person.h"
#include "../optionals.h"
#include <tuple>
#include "common.h"

static const auto num_sexes = static_cast<size_t>(faker::person::Sex::COUNT);

static auto default_params(std::optional<faker::DataComplexity> dataComplexity, std::optional<faker::person::Sex> sex,
                           std::optional<size_t> index) -> std::tuple<faker::DataComplexity, faker::person::Sex, size_t> {
    auto [complexity, dataIndexTpl] = faker::utils::init_complexity(dataComplexity, index);
    auto dataIndex = dataIndexTpl;

    auto gender = std::min(faker::opts::or_else(sex, [&dataIndex](){
        auto res = static_cast<faker::person::Sex>(dataIndex % num_sexes);
        dataIndex /= num_sexes;
        return res;
    }), static_cast<faker::person::Sex>(static_cast<int>(faker::person::Sex::COUNT) - 1));

    return {complexity, gender, dataIndex};
}

auto faker::person::given_name(std::optional<DataComplexity> dataComplexity, std::optional<Sex> sex,
                               std::optional<size_t> index) -> std::string {
    const auto [complexity, gender, dataIndex] = default_params(dataComplexity, sex, index);
    return data::person::given_name(complexity, gender, dataIndex);
}

auto faker::person::surname(std::optional<DataComplexity> dataComplexity,
                            std::optional<size_t> index) -> std::string {
    const auto [complexity, dataIndex] = faker::utils::init_complexity(dataComplexity, index);
    return data::person::surname(complexity, dataIndex);
}

auto faker::person::suffix(std::optional<DataComplexity> dataComplexity, std::optional<Sex> sex,
                           std::optional<size_t> index) -> std::optional<std::string> {
    const auto [complexity, gender, dataIndex] = default_params(dataComplexity, sex, index);
    if (dataIndex % 13 == 1) {
        return std::nullopt;
    }
    return data::person::suffix(complexity, gender, dataIndex);
}

auto faker::person::prefix(std::optional<DataComplexity> dataComplexity, std::optional<Sex> sex,
                           std::optional<size_t> index) -> std::optional<std::string> {
    const auto [complexity, gender, dataIndex] = default_params(dataComplexity, sex, index);
    if (dataIndex % 7 == 2) {
        return std::nullopt;
    }
    return data::person::prefix(complexity, gender, dataIndex);
}

auto faker::person::full_name(std::optional<DataComplexity> dataComplexity, std::optional<Sex> sex,
                              std::optional<size_t> index) -> std::string {
    const auto [complexity, gender, dataIndex] = default_params(dataComplexity, sex, index);
    auto givenName = data::person::given_name(complexity, gender, dataIndex);

    auto surName = data::person::surname(complexity, dataIndex);

    bool reverseNames = (dataComplexity >= DataComplexity::ADVANCED && (dataIndex & 0x0010000));

    if (reverseNames) {
        return std::string{surName} + " " + givenName;
    }
    return std::string{givenName} + " " + surName;
}

auto faker::person::person(std::optional<DataComplexity> dataComplexity, std::optional<Sex> sex,
                                   std::optional<size_t> index) -> faker::person::Person {
    auto [complexity, gender, dataIndex] = default_params(dataComplexity, sex, index);

    return {
        .prefix = faker::person::prefix(complexity, gender, dataIndex),
        .suffix = faker::person::suffix(complexity, gender, dataIndex),
        .givenName = given_name(complexity, gender, dataIndex),
        .surName = surname(complexity, dataIndex),
        .fullName = full_name(complexity, gender, dataIndex),
        .sex = gender
    };
}
