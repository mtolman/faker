#include <faker/world.h>
#include "common.h"
#include "../data/country.h"

auto faker::world::country(std::optional<size_t> index) -> Country {
    auto i = faker::utils::init_index(index);
    return faker::data::country::country(i);
}
