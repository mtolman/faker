#include <faker/malicious.h>
#include "../data/malicious.h"
#include "common.h"

auto faker::malicious::attack(const std::optional<ATTACK_TYPE>& attackType, std::optional<size_t> index) -> std::string {
    const auto i = faker::utils::init_index(index);
    auto type = static_cast<ATTACK_TYPE>(i % static_cast<int>(ATTACK_TYPE::COUNT));
    return faker::data::malicious::get_attack(type, i);
}

