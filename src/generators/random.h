#pragma once

#include <random>
#include <limits>

namespace faker::random {
    struct Random {
        template <typename T>
        static auto integer(T min, T max) -> T {
            static_assert(std::numeric_limits<T>::is_integer);
            if (min > max) {
                std::swap(min, max);
            }

            return std::uniform_int_distribution<T>{min, max}(random);
        }

        template <typename T>
        static auto integer(T max) -> T {
            return integer(static_cast<T>(0), max);
        }

        template <typename T>
        static auto integer() -> T { return integer(static_cast<T>(0), std::numeric_limits<T>::max()); }
    private:
        static std::mt19937 random;
    };
}
