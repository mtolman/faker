#include <faker/color.h>
#include "common.h"
#include <random>
#include <sstream>

template<size_t N = 3>
static auto inline generate_doubles(std::mt19937_64& rnd) -> std::array<double, N> {
    auto dst = std::uniform_real_distribution(0.0, 1.0);
    auto res = std::array<double, N>{0};
    for (auto& c : res) {
        c = dst(rnd);
    }
    return res;
}

template<typename T>
static auto inline double_to_range(double d) -> T {
    return static_cast<T>(
            std::round(static_cast<double>(std::numeric_limits<T>::max()) * d)
    );
}

static auto to_hex(std::ostream& os, uint8_t d) {
    static constexpr auto strs = "0123456789ABCDEF";
    auto lower = d & 0b1111;
    auto upper = (d >> 4) & 0b1111;
    os << strs[upper] << strs[lower];
}

static auto percentage(std::ostream& os, double d) {
    os << static_cast<int>(std::round(d * 100)) << "%";
}

static auto degree(std::ostream& os, double d) {
    os << static_cast<int>(std::round(d * 360));
}

auto faker::color::rgb(std::optional<size_t> index) -> faker::color::RGB {
    auto [_, rnd] = faker::utils::init_rand(index);
    const auto res = generate_doubles(rnd);
    return faker::color::RGB{ res[0], res[1], res[2], };
}

auto faker::color::RGB::css() const -> std::string {
    std::stringstream ss{};
    const auto vals = bytes();
    ss << "rgb(" << static_cast<int>(vals[0])
       << ", " << static_cast<int>(vals[1])
       << ", " << static_cast<int>(vals[2]) << ")";
    return ss.str();
}


auto faker::color::RGB::hex(const std::string& prefix) const -> std::string {
    std::stringstream ss;
    ss << prefix;
    const auto vals = bytes();
    to_hex(ss, vals[0]);
    to_hex(ss, vals[1]);
    to_hex(ss, vals[2]);
    return ss.str();
}

auto faker::color::RGB::bytes() const -> std::array<uint8_t, 3> {
    return std::array<uint8_t, 3>{
            double_to_range<uint8_t>(red),
            double_to_range<uint8_t>(green),
            double_to_range<uint8_t>(blue),
    };
}

auto faker::color::rgba(std::optional<size_t> index) -> faker::color::RGBA {
    auto [_, rnd] = faker::utils::init_rand(index);
    const auto vals = generate_doubles<4>(rnd);
    return faker::color::RGBA{ vals[0], vals[1], vals[2], vals[3] };
}

auto faker::color::RGBA::css() const -> std::string {
    const auto vals = bytes();
    std::stringstream ss{};
    ss << "rgba("
       << static_cast<int>(vals[0]) << ", "
       << static_cast<int>(vals[1]) << ", "
       << static_cast<int>(vals[2]) << ", "
       << static_cast<int>(vals[3]) << ")";
    return ss.str();
}


auto faker::color::RGBA::hex(const std::string &prefix) const -> std::string {
    std::stringstream ss;
    const auto vals = bytes();
    ss << prefix;
    to_hex(ss, vals[0]);
    to_hex(ss, vals[1]);
    to_hex(ss, vals[2]);
    to_hex(ss, vals[3]);
    return ss.str();
}

auto faker::color::RGBA::bytes() const -> std::array<uint8_t, 4> {
    return {
            double_to_range<uint8_t>(red),
            double_to_range<uint8_t>(green),
            double_to_range<uint8_t>(blue),
            double_to_range<uint8_t>(alpha),
    };
}

auto faker::color::hsl(std::optional<size_t> index) -> faker::color::HSL {
    auto [_, rnd] = faker::utils::init_rand(index);
    const auto res = generate_doubles(rnd);
    return {res[0] * 360.0, res[1], res[2]};
}

auto faker::color::HSL::css() const -> std::string {
    std::stringstream ss;
    ss << "hsl(" << static_cast<int>(std::round(hue)) << ", ";
    percentage(ss, saturation);
    ss << ", ";
    percentage(ss, luminosity);
    ss << ")";
    return ss.str();
}

auto faker::color::hsla(std::optional<size_t> index) -> faker::color::HSLA {
    auto [_, rnd] = faker::utils::init_rand(index);
    auto res = generate_doubles<4>(rnd);
    return {res[0] * 360.0, res[1], res[2], res[3]};
}

auto faker::color::HSLA::css() const -> std::string {
    std::stringstream ss;
    ss << "hsla(" << static_cast<int>(std::round(hue)) << ", ";
    percentage(ss, saturation);
    ss << ", ";
    percentage(ss, luminosity);
    ss << ", ";
    ss << alpha;
    ss << ")";
    return ss.str();
}
