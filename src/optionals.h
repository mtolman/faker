#pragma once

#include <optional>
#include <functional>

namespace faker::opts {
    template<typename T, typename F>
    auto or_else(const std::optional<T> &v, const F &f) -> T {
        if (v.has_value()) {
            return *v;
        }
        return f();
    }
}
