#pragma once

#include <faker/finance.h>

namespace faker::data {
    auto currency(size_t index) -> const faker::finance::Currency&;
}
