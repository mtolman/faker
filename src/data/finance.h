#pragma once

#include <cstddef>
#include "faker/common.h"
#include <string>

namespace faker::data {
    auto account_name(faker::DataComplexity complexity, size_t index) -> std::string;
}
