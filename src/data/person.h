#pragma once

#include <faker/common.h>
#include <faker/person.h>
#include <vector>
#include <optional>
#include <string>

namespace faker::data::person {
    auto given_name(DataComplexity dataComplexity, faker::person::Sex sex, size_t id) -> std::string;
    auto surname(DataComplexity dataComplexity, size_t id) -> std::string;
    auto prefix(DataComplexity dataComplexity, faker::person::Sex sex, size_t id) -> std::optional<std::string>;
    auto suffix(DataComplexity dataComplexity, faker::person::Sex sex, size_t id) -> std::optional<std::string>;
}
