#include "finance.h"
#include <map>
#include <vector>
#include <array>

static const std::map<faker::DataComplexity, std::vector<std::string>> account_name_data = {
        {faker::DataComplexity::RUDIMENTARY,  {
                                                      "Checking Account",
                                                      "Checking",
                                                      "Personal Account",
                                                      "Money Market",
                                                      "Investment Account",
                                                      "Savings",
                                                      "Savings Account",
                                                      "Home Loan",
                                                      "Mortgage Account",
                                                      "Escrow",
                                                      "Credit Card",
                                                      "Auto Loan",
                                                      "Personal Loan",
                                                      "Student Loan",
                                                      "Student Loan Account"
                                              }},
        {faker::DataComplexity::BASIC,        {
                                                      "Checking -- 7534",
                                                      "Savings -- 7562",
                                                      "Discover Card -- 1423",
                                                      "Discover Credit Card -- 1453",
                                                      "Master Card -- 2948",
                                                      "Student Loan # 92483",
                                                      "Auto Loan # 47583",
                                                      "Personal Loan # 55938",
                                              }},
        {faker::DataComplexity::INTERMEDIATE, {
                                                      "Checking [7534]",
                                                      "Savings {#7562}",
                                                      "Discover Card -- _1423_",
                                                      "Discover Credit Card #####1453",
                                                      "Master Card ****2948",
                                                      "Student Loan **92483**",
                                                      "Auto Loan @47583",
                                                      "Personal Loan @55938@",
                                              }},
        {faker::DataComplexity::ADVANCED,     {
                                                      "Compte d\u2019\u00E9pargne",
                                                      "Provjera ra\u010Duna",
                                                      "Investicioni ra\u010Dun",
                                                      "Carte de cr\u00E9dit",
                                                      "Carte de cr\u00E9dit -- 2948",
                                                      "Carte de cr\u00E9dit # 92483",
                                                      "Compte hypoth\u00E9caire # 47583",
                                              }},
        {faker::DataComplexity::COMPLEX,      {
                                                      "\u652F\u7968\u8D26\u6237",
                                                      "\u05D7\u05E9\u05D1\u05D5\u05DF \u05D7\u05D9\u05E1\u05DB\u05D5\u05DF",
                                                      "\u0628\u0637\u0627\u0642\u0629 \u0627\u0644\u0625\u0626\u062A\u0645\u0627\u0646",
                                                      "\u6309\u63ED\u5E33\u6236",
                                                      "\u6309\u63ED\u6236\u53E3 - 2493",
                                                      "\uBAA8\uAE30\uC9C0 \uACC4\uC88C - 2493",
                                                      "\u5B66\u751F\u30ED\u30FC\u30F3\u53E3\u5EA7-4939",
                                              }}
};

auto faker::data::account_name(faker::DataComplexity complexity, size_t index) -> std::string {
    const auto &vec = account_name_data.find(complexity)->second;
    return vec[index % vec.size()];
}