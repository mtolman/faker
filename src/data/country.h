#pragma once

#include <cstddef>
#include <optional>
#include <faker/world.h>

namespace faker::data::country {
    auto country(size_t index) -> const faker::world::Country&;
}
