#pragma once

#include <string>
#include <vector>
#include <faker/malicious.h>

namespace faker::data::malicious {
    auto get_attack(faker::malicious::ATTACK_TYPE attackType, size_t index) -> std::string;
}