# Faker (C++)

Inspired by [faker-js](https://fakerjs.dev/) and [faker-cxx](https://github.com/cieslarmichal/faker-cxx).
However, unlike both faker-js and faker-cxx, this library is meant for property
testing. The need for property testing requires there to be determinism and
reproducibility of the output, that way the property tests can fail with the same
seed. The library is also setup to handle "shrinking" by dividing the data into
categories of properties rather than locales (e.g. requires basic ASCII support,
requires full unicode support, etc).

This library also includes "malicious" input samples (XSS, SQL, etc.). These samples
are not part of the default generators to avoid unintentional damage when running
for the first time. These malicious input samples can be potent, so it is advised
to be careful with them. The malicious samples are provided as a tool to help
secure software that you write; they are not provided to hack, attack, destroy,
manipulate, gain unauthorized access, etc. software which you do not own or have
permission to run these samples on. Be responsible.

## Deterministic Generation

Every method optionally accepts an "index", which is basically the seed you want
to use. Everything is deterministic, so when you pass the same seed multiple times,
you get the same value.

If an index is not specified, then a random value will be generated. The random
value uses a singleton instance of Mersenne Twister. We do not use a
cryptographically secure random number generator.

There is no thread safety mechanism around our singleton instance. This means
you could have race conditions if multiple thread rely on our libraries' values.
If you need multithreading support, we strongly recommend that you create your
own thread-safe random number generator, and then pass in the values from that
generator into our methods.

## Data Complexity

The data complexity metric is based off of my experience working with software in
the U.S. market. I created a lot of data that represents the capabilities I've seen
and realistic data which I've seen appear throughout my career. I divided the data into
groups which represent how well (or poorly) many of the different programs I've 
worked with (used and developed) could handle that type of input. I then trimmed the
down to a much smaller data set and put it in this library. The goal is not to
prove how well a program handles data, but rather to show if there are any glaring
weaknesses.

I have split the data into the following categories:
* Rudimentary
* Basic
* Intermediate
* Advanced
* Complex

Each level gets progressively harder to write code which can properly handle that
level. We go from "interns can't mess this up" to "principal engineers sometimes
get this wrong."

### Rudimentary

The rudimentary level is the most basic level to handle. It's good data. It's the
happy case. No edge cases here. The generated data isn't ridiculously long, or
hard, or anything. This is the "happy, pleasant to look at user data".

For instance, almost every web application can handle users' names that strictly
match the form `/[a-zA-Z]+/` without issue. So, for the "rudimentary" level, only
names matching the form `/[a-zA-Z]+/` will appear.

The goal of the rudimentary level isn't to prove correctness. It's to help know
how awful the code is. It's the baseline. It's meant to help determine if the
problem is with a specific class of inputs, or if it's a problem with any type of
input.

This is the "have we horrifically screwed up our code" level.

### Basic

This is the basic level where software *should* pass it, but sometimes it doesn't.
This has the most very basic edge cases. Failing this is embarrassing, and it will
impact users in English speaking markets. Any code which fails this level is bad
code.

For instance, some English names have apostrophes (O'Conner), others have hyphens
(Gutierrez-Gonzalez), and some have spaces (Fernando Gomez). I personally know
people with in all three of these scenarios. They are negatively impacted by a lot
of bad code. Any code which cannot handle these names is bad code.

It's also not hard to properly support these names. Use prepared SQL statements
to handle apostrophes, don't split names on spaces, etc.

It amazes me how often software screws up this level. It also amazes me how often
software screws up this level, but then passes one of the "harder" levels. It's
like software is developed in an isolated "bubble" where they can't even support
their primary market (U.S.) but they do spend time trying to support some other
market that they have no clue how it operates.

Usually, I find this level is violated because somebody didn't want to use
prepared statements with their SQL queries. Instead, they remove or block
apostrophes while they allow everything else through (which is still a terrible
idea). However, that does mean that they cannot support the entire U.S. market
(a lot of people have names like "O'Neal" and "O'Connor"). Supporting this level
requires doing basic stuff, so it's basic text processing.

This is the "are we actually ready for the U.S. market" level.

### Intermediate

This level is mostly for weird hacks I've seen users do to get around limitations
of a platform. The creativity of users is very impressive. I've only captured a
few examples.

For instance, let's consider the name field. It's surprising how much information
B2B users will encode into a name field. I've seen account numbers, nicknames,
debt statuses (e.g. paid, behind), and so much more put into name fields. Generally,
any information that the business employees want to see at a quick glance gets
put in the name field. Sometime later, the software company will sometimes
realize they should add dedicated columns for some of these fields, but that's
usually well after this hack has been firmly established.

Surprisingly, quite a bit of software will be able to pass this level. Mostly
since it represents weird hacks users have done to "enhance" said software.

However, I have had to work with software which cannot pass this level of support.
It's often very painful since it's generally in the context of "this platform can
support these hacks, but it needs to integrate into this platform which doesn't."
Usually the workaround is to strip out characters that cause integration issues.

This is the "can we integrate with other U.S. software" level.

### Advanced

Now we start entering basic Unicode support. I say "basic" since this level does
not require full (or even correct) Unicode support. Instead, most broken "unicode"
implementations can handle this kind of text (e.g. UCS-2, MySQL's utf8, etc).

For names, this includes names with accented characters, such as José. Most
developers who can read ASCII can read these names just fine, and many will be
able to debug and "intuition" their way through lots of problems (e.g. sorting).

A lot of software gets past this tier with NFD Unicode normalization followed by
ASCII operations (change case, sort, etc). It's not perfect, but often good enough.

This is the "can we support North America and Western Europe" level.

### Complex

This section requires full unicode support. No UCS-2, no "broken" encodings. Only
the full support goes here. Sorting requires full, proper collation to get it
correct. This means proper UTF-8, UTF-16, and/or UTF-32 (for MySQL, use utf8mb4).

For names, these are non-Latin names, such as 志旼 and ຄຳເພັດ. Many devs
have never seen these names and know very little about how they would be properly
sorted or modified. Many devs would be at a loss for how to even pronounce them.

This is the "can we support more than just North America and Western Europe" level.

Ideally, all software woul strive to reach this level of support.

## Malicious Data Set

**WARNING:** Do not run this data set on a computer system without permission!
Some of these attacks are potent, meaning they can cause damage when ran using
insecure code!

**Do not use this data set on systems where you are not authorized to do so!**

**Do not use this data set to hack, take down, or harm computer systems you do not
own or have permissions to do security testing**

> **Important!** Not being negatively impacted by this data set does not mean you
> are secure! It is merely one tool to help with testing. However, it is a tool that
> is blunt. It does not know what your code does, it does not know which programming
> language you use, it does not know how your servers are set. A lot more work needs
> to go into testing your program past this data set to know if you're secure. This
> data set is merely to check if you've covered your basics.

This data set is made to break your application. It is full of edge cases which
have caused breakages in many programs (including ones that I have developed). It
also includes potent sample attacks adapted from security training material (e.g.
OWASP).

> Be careful when running this data set, especially for the first time. If
> you've never done any security testing on your program, then expect it to misbehave.
> There may be corrupted data in your database, there may be JavaScript alerts injected
> in your UI, there may be tables dropped your DB. Files on your server may be leaked.
> Only run these in a controlled environment where you will not corrupt, lose, or leak
> customer information.  

There are currently several different bands of malicious inputs: XSS, SQL Injection,
Format Injection, Edge Cases.

### XSS

This data set is for Cross Site Scripting attacks (XSS). It will try to inject
JavaScript code in a variety of ways, including:
* Straight up JS injection with a `<script>` tag
* Escaping out of attributes
* Injecting with `<meta>` tags using base64 data
* Adding JS event handlers to "benign" tags (e.g. `<b>`)
* Recursive nesting of tags to bypass tag removal (e.g. `<scr<script>pt>`)
* URL encoding of tags (e.g. `%3cscript%3e`)
* JS encoding of tags (e.g. `\u003cscript\u003e`)
* HTML encoding of `javascript` in an attribute (e.g. `j&#X41vascript:`)
* HTML encoding tags (to try to exploit html decoding)

Most of these attacks will try to load a non-existent JS file or do an alert.

### SQL Injection

This data set will try various ways to inject malicious SQL. Some of these attacks
will simply return data it shouldn't. Others will try to load files from disk,
do a DOS attack, drop tables, shut down the database, drop schemas or rows, etc.

It is very important that you are careful with this data set. If your code is
properly using prepared statements, there should be no negative impact from this
data set. However, if you are vulnerable to SQL injection, then this data set
will make it very obvious by destroying your database. Do not run this in
production!

### Format Injection

This data set will try to inject malicious code into formatting functions (e.g.
`printf`). It doesn't have a lot of examples, but it's not just limited to C's
`printf`. It does include other types of injection attacks, such as ones
for python and some [Log4Shell](https://en.wikipedia.org/wiki/Log4Shell) attacks
for Java.

### Edge Cases

These are edge cases which tend to cause program errors and/or crashes. These
data points often happen as a result of bugs or errors and not as much from
hackers trying to hack your program. Most commonly, they happen as a result of
clients and servers using different encodings, data getting corrupted in-transit,
etc.

That said, I have seen these data inputs cause some pretty nasty bugs, mixed in
with the occasional data corruption. For that reason, I put them in the
"malicious" package since they can be dangerous to programs, and intentionally
sending them to other programs could cause damage.

## Performance

This code is not the most performant. It's good enough for running tests. It's good
enough for populating small databases with sample data. It's not good enough for
high performance applications that (for some reason) have to generate loads of fake
data extremely quickly. This library doesn't do that. We prioritized flexibility
in features over high performance.

For instance, take a look at our ascii text generation algorithm for alphabetical
strings:

```cpp
auto faker::generators::alpha_str(const faker::ascii::AlphaOpts &opts, std::mt19937_64 &rnd) -> std::string {
    if (opts.length == 0) {
        return "";
    }

    auto exclusions = opts.exclude.value_or(std::set<char>{});
    if (opts.casing == ascii::Casing::UPPER) {
        exclusions.merge(std::set<char>{'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z'});
    }

    if (opts.casing == ascii::Casing::LOWER) {
        exclusions.merge(std::set<char>{'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'});
    }

    std::stringstream ss;
    auto len = opts.length;

    std::optional<std::vector<char>> possible = std::nullopt;
    if (!exclusions.empty()) {
        possible = alpha_make_possibilities(exclusions);
    }

    while (len-- > 0) {
        ss << alpha_char(possible, rnd);
    }
    return ss.str();
}

static const auto alphaChars = std::array{
    'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o',
    'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'A', 'B', 'C', 'D',
    'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S',
    'T', 'U', 'V', 'W', 'X', 'Y', 'Z',
};

static auto alpha_make_possibilities(const std::set<char>& exclude) -> std::vector<char> {
    auto fullSet = std::vector<char>{};
    for (const auto& c : alphaChars) {
        if (exclude.find(c) == exclude.end()) {
            fullSet.push_back(c);
        }
    }
    return fullSet;
}

static auto alpha_char(const std::optional<std::vector<char>> &possibilities, std::mt19937_64 &rnd) -> char {
    if (!possibilities || possibilities->empty()) {
        return alphaChars[std::uniform_int_distribution<size_t>(0, alphaChars.size() - 1)(rnd)];
    }

    return possibilities->at(std::uniform_int_distribution<size_t>(0, possibilities->size() - 1)(rnd));
}
```

There's quite a bit slow about the above. For one, we're doing a lot of memory
allocations with sets. We're also using ordered sets, which are generally tree-based.
We're also copying data out of an array and into a set. There's a lot going on.

However, the above allows us to exclude any number of characters from our output
set. We could have done an "inclusion" set, but we chose "exclusion" since that's
the model faker-js uses, and we wanted to stay somewhat close to the usage that
devs would find in similar libraries. That choice made our code more complicated
since we have to go from a superset down to a subset. It also makes the code slower.

If we wanted highly performant code, we could remove the ability for customizing
the set and write the following:

```cpp
auto faker::generators::alpha_str(const faker::ascii::AlphaOpts &opts, std::mt19937_64 &rnd) -> std::string {
    // Single memory allocation
    std::string res(opts.length, '\0');
    if (opts.casing == CASING::LOWERCASE) {
        for (char& ch : res) {
            // Use basic math, no memory lookups or jumps
            ch = 'a' + rnd() % 26;
        }
    }
    else if (opts.casing == CASING::UPPERCASE) {
        for (char& ch : res) {
            ch = 'A' + rnd() % 26;
        }
    }
    else {
        auto caseDist = std::uniform_distribution(0, 1);
        for (char& ch : res) {
            // Using basic math and ASCII properties again
            // The difference between upper and lower ASCII is a single bit
            // We conditionally set our bit based on the random number we got
            // We use a binary and operator to conditionally carry our bit through
            ch = ('A' + rnd() % 26) | (rnd & 0b00100000);
        }
    }
    return res;
}
```

The above code is going to be much faster than what we have in this library. If
we still wanted to allow customization, we could do something fairly quick that's
just a memory lookup by using "inclusion" rather than "exclusion".

```cpp
struct AlphaOpts {
        size_t length = 3;
        std::vector<char> include = { /* ... alphabet here ... */ };
};

auto faker::generators::alpha_str(const faker::ascii::AlphaOpts &opts, std::mt19937_64 &rnd) -> std::string {
    // Single memory allocation
    std::string res(opts.length, '\0');
    if (opts.include.empty()) {
        return res;
    }
    for (char& ch : res) {
        // Use basic math, no memory lookups or jumps
        ch = opts.include[rnd() % opts.include.size()];
    }
    return res;
}
```

Much simpler code, a lot quicker, and we can get rid of the casing variable since
we can just define `UPPER` and `LOWER` case variables. However, it doesn't match
the interface from faker-js, so we aren't using it.
