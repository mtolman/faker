#pragma once

#include <ostream>
#include "../data.h"

namespace rc::detail {
    inline void showValue(const faker::rc::malicious::SqlInjectionAttack& d, std::ostream& os) {
        os << "SqlInjectionAttack{\n\t" << d.value << "\n}";
    }

    inline void showValue(const faker::rc::malicious::XssAttack& d, std::ostream& os) {
        os << "XssAttack{\n\t" << d.value << "\n}";
    }

    inline void showValue(const faker::rc::malicious::FormatInjectionAttack& d, std::ostream& os) {
        os << "FormatInjectionAttack{\n\t" << d.value << "\n}";
    }

    inline void showValue(const faker::rc::malicious::EdgeCase& d, std::ostream& os) {
        os << "EdgeCase{\n\t" << d.value << "\n}";
    }
}
