#pragma once

#include <faker/common.h>
#include <ostream>

namespace rc::detail {
    inline void showValue(const ::faker::DataComplexity &dataComplexity, std::ostream &os) {
        switch (dataComplexity) {
            case faker::DataComplexity::RUDIMENTARY:
                os << "DataComplexity::RUDIMENTARY";
                break;
            case faker::DataComplexity::BASIC:
                os << "DataComplexity::BASIC";
                break;
            case faker::DataComplexity::INTERMEDIATE:
                os << "DataComplexity::INTERMEDIATE";
                break;
            case faker::DataComplexity::ADVANCED:
                os << "DataComplexity::ADVANCED";
                break;
            case faker::DataComplexity::COMPLEX:
                os << "DataComplexity::COMPLEX";
                break;
            case faker::DataComplexity::COUNT:
                os << "DataComplexity::COUNT";
                break;
        }
    }
}
