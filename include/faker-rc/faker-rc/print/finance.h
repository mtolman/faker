#pragma once

#include <ostream>
#include "../data.h"

namespace rc::detail {
    template<size_t MinLength, size_t MaxLength>
    void showValue(const faker::rc::finance::AccountNumber<MinLength, MaxLength>& d, std::ostream& os) {
        os << "Account#" << d.value;
    }

    template<faker::DataComplexity MaxComplexity>
    void showValue(const faker::rc::finance::AccountName<MaxComplexity>& d, std::ostream& os) {
        os << "Account:\"" << d.value << '"';
    }

    template<
            unsigned DecimalPlaces,
            int Max,
            int Min,
            typename Symbol,
            char DecimalSeparator
    >
    void showValue(
            const faker::rc::finance::Amount<faker::rc::finance::AmountSymOptions<DecimalPlaces, Max, Min, Symbol, DecimalSeparator>>& d,
            std::ostream& os) {
        os << "Amount<"
           << "decimalPlaces=" << DecimalPlaces
           << ", decSeparator=" << DecimalSeparator
           << ", max=" << Max
           << ", min=" << Min
           << ", symbol=Sym("
           << "exists=" << Symbol::exists
           << ", pos=" << (Symbol::position == faker::finance::CurrencySymbolPos::PREFIX ? "PREFIX" : "SUFFIX")
           << ", str=" << Symbol::str
           << ")"
           << ">:"
           << d.value
           << '"';
    }

    template<
            unsigned DecimalPlaces,
            int Max,
            int Min,
            faker::finance::CurrencySymbolPos Pos,
            char DecimalSeparator
    >
    void showValue(
            const faker::rc::finance::Amount<faker::rc::finance::AmountRandomCurrency<DecimalPlaces, Max, Min, Pos, DecimalSeparator>>& d,
            std::ostream& os) {
        os << "Amount<"
           << "decimalPlaces=" << DecimalPlaces
           << ", decSeparator=" << DecimalSeparator
           << ", max=" << Max
           << ", min=" << Min
           << ", symbol=Random(pos=" << (Pos == faker::finance::CurrencySymbolPos::PREFIX ? "PREFIX" : "SUFFIX") << ")"
           << ">:"
           << d.value
           << '"';
    }

    inline void showValue(const faker::rc::finance::TransactionType & d, std::ostream& os) {
        os << "TransactionType::";
        switch (d) {
            case faker::finance::TransactionType::DEPOSIT:
                os << "DEPOSIT";
                break;
            case faker::finance::TransactionType::WITHDRAWAL:
                os << "WITHDRAWAL";
                break;
            case faker::finance::TransactionType::PAYMENT:
                os << "PAYMENT";
                break;
            case faker::finance::TransactionType::INVOICE:
                os << "INVOICE";
                break;
            case faker::finance::TransactionType::COUNT:
                os << "COUNT";
                break;
        }
    }

    inline void showValue(const faker::rc::finance::CurrencySymbolPos & d, std::ostream& os) {
        os << "CurrencySymbolPos::";
        switch (d) {
            case faker::finance::CurrencySymbolPos::PREFIX:
                os << "PREFIX";
                break;
            case faker::finance::CurrencySymbolPos::SUFFIX:
                os << "SUFFIX";
                break;
        }
    }

    inline void showValue(const faker::rc::finance::IncludeBranch & d, std::ostream& os) {
        os << "IncludeBranch::";
        switch (d) {
            case faker::rc::finance::IncludeBranch::ALWAYS:
                os << "ALWAYS";
                break;
            case faker::rc::finance::IncludeBranch::NEVER:
                os << "NEVER";
                break;
            case faker::rc::finance::IncludeBranch::SOMETIMES:
                os << "SOMETIMES";
                break;
        }
    }

    template<faker::rc::finance::IncludeBranch BC>
    inline void showValue(const faker::rc::finance::Bic<BC> & d, std::ostream& os) {
        os << "Bic<branchCode=";
        showValue(BC, os);
        os << ">{" << d.value << "}";
    }

    template<
            size_t UnmaskedLen,
            size_t MaskedLen,
            char MaskChar,
            bool SurroundWithParens
    >
    inline void showValue(const faker::rc::finance::MaskedNumber<UnmaskedLen, MaskedLen, MaskChar, SurroundWithParens> & d, std::ostream& os) {
        os << "MaskedNumber<unmaskedLen=" << UnmaskedLen
           << ", maskedLen=" << MaskedLen
           << ", maskedChar=" << MaskChar
           << ", surroundWithParens=" << SurroundWithParens
           << ">{" << d.value << "}";
    }

    template<
            size_t MinLen,
            size_t MaxLen
    >
    inline void showValue(const faker::rc::finance::Pin<MinLen, MaxLen> & d, std::ostream& os) {
        os << "Pin<minLen=" << MinLen << ", maxLen=" << MaxLen
           << ">{" << d.value << "}";
    }

    inline void showValue(faker::finance::credit_card::Network n, std::ostream& os) {
        os << "NETWORK::";
        switch(n) {
            case faker::finance::credit_card::Network::AMERICAN_EXPRESS:
                os << "AMERICAN_EXPRESS";
                break;
            case faker::finance::credit_card::Network::DINERS:
                os << "DINERS";
                break;
            case faker::finance::credit_card::Network::DISCOVER:
                os << "DISCOVER";
                break;
            case faker::finance::credit_card::Network::ENROUTE:
                os << "ENROUTE";
                break;
            case faker::finance::credit_card::Network::JCB15:
                os << "JCB15";
                break;
            case faker::finance::credit_card::Network::JCB16:
                os << "JCB16";
                break;
            case faker::finance::credit_card::Network::MASTERCARD:
                os << "MASTERCARD";
                break;
            case faker::finance::credit_card::Network::VOYAGER:
                os << "VOYAGER";
                break;
            case faker::finance::credit_card::Network::VISA:
                os << "VISA";
                break;
            case faker::finance::credit_card::Network::COUNT:
                os << "COUNT";
                break;
        }
    }

    inline void showValue(const faker::rc::finance::CreditCard & d, std::ostream& os) {
        os << "CreditCard{"
           << "cvv=" << d.cvv
           << ", network=";
        showValue(d.network, os);
        os << ", number=" << d.number
           << ", expiration=" << d.expiration
           << "}";
    }

    inline void showValue(const faker::rc::finance::Currency & d, std::ostream& os) {
        os << "Currency{"
           << "code=" << d.code
           << ", name=" << d.name
           << ", symbol=" << d.symbol
           << "}";
    }
}
