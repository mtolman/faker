#pragma once

#include "../data.h"
#include <ostream>

namespace rc::detail {
    inline void showValue(const faker::rc::world::Country& d, std::ostream& os) {
        os << "Country{"
           << "\n\t .name=" << d.name
           << "\n\t .alpha2=" << d.alpha2
           << "\n\t .alpha3=" << d.alpha3
           << "\n\t .number=" << d.numericCode
           << "\n\t .domain=" << d.domain.value_or("<none>")
           << "\n}";
    }
}
