#pragma once

#include <ostream>
#include "../data.h"

namespace rc::detail {
    template<size_t Length, bool AllowLeadingZeros = true, char... Excluded>
    void showValue(const faker::rc::ascii::Numeric<Length, AllowLeadingZeros, Excluded...>& d, std::ostream& os) {
        os << '"' << d.value << '"';
    }

    template<size_t Length, faker::ascii::Casing Casing = faker::ascii::Casing::MIXED, char... Excluded>
    void showValue(const faker::rc::ascii::Alpha<Length, Casing, Excluded...>& d, std::ostream& os) {
        os << '"' << d.value << '"';
    }

    template<size_t Length, faker::ascii::Casing Casing = faker::ascii::Casing::MIXED, char... Excluded>
    void showValue(const faker::rc::ascii::AlphaNumeric<Length, Casing, Excluded...>& d, std::ostream& os) {
        os << '"' << d.value << '"';
    }

    template<size_t Length, faker::ascii::Casing Casing = faker::ascii::Casing::MIXED>
    void showValue(const faker::rc::ascii::Hex<Length, Casing>& d, std::ostream& os) {
        os << '"' << d.value << '"';
    }
}