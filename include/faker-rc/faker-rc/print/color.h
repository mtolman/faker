#pragma once

#include "../data.h"

namespace rc::detail {
    inline void showValue(const faker::rc::color::RGB &d, std::ostream &os) {
        os << "Color:" << d.css();
    }
    inline void showValue(const faker::rc::color::RGBA &d, std::ostream &os) {
        os << "Color:" << d.css();
    }
    inline void showValue(const faker::rc::color::HSL &d, std::ostream &os) {
        os << "Color:" << d.css();
    }
    inline void showValue(const faker::rc::color::HSLA &d, std::ostream &os) {
        os << "Color:" << d.css();
    }
}
