#pragma once

#include <faker/person.h>
#include <ostream>
#include "../data.h"

namespace rc::detail {
    inline void showValue(const ::faker::person::Person &p, std::ostream &os) {
        os << "Person{\n\t" <<
           ".prefix=\"" << p.prefix.value_or("std::nullopt") << "\"\n\t" <<
           ".suffix=\"" << p.suffix.value_or("std::nullopt") << "\"\n\t" <<
           ".givenName=\"" << p.givenName << "\"\n\t" <<
           ".surName=\"" << p.surName << "\"\n\t" <<
           ".fullName=\"" << p.fullName << "\"\n\t" <<
           ".sex=SEX::" << (p.sex == faker::person::Sex::FEMALE ? "FEMALE" : "MALE") << "\n}";
    }

    inline void showValue(const ::faker::person::Sex &s, std::ostream &os) {
        os << (s == faker::person::Sex::FEMALE ? "FEMALE" : "MALE");
    }

    template<faker::DataComplexity MaxComplexity>
    void showValue(const ::faker::rc::person::Person<MaxComplexity> &p, std::ostream &os) {
        os << "<";
        showValue(MaxComplexity, os);
        os << ">";
        showValue(*p, os);
    }
}
