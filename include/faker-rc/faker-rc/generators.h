#pragma once

#include "generators/common-generators.h"
#include "generators/ascii.h"
#include "generators/color.h"
#include "generators/finance.h"
#include "generators/malicious.h"
#include "generators/person.h"
#include "generators/world.h"
