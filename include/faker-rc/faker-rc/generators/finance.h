#pragma once

#include "../data/finance.h"
#include "common-generators.h"
#include <rapidcheck.h>

static_assert(rc::detail::HasShowValue<faker::rc::finance::AccountNumber<4>>::value,
              "Make sure faker-rc/print.h is included first");
static_assert(rc::detail::HasShowValue<faker::rc::finance::AccountName<>>::value,
              "Make sure faker-rc/print.h is included first");
static_assert(rc::detail::HasShowValue<faker::rc::finance::TransactionType>::value,
              "Make sure faker-rc/print.h is included first");
static_assert(rc::detail::HasShowValue<faker::rc::finance::CurrencySymbolPos>::value,
              "Make sure faker-rc/print.h is included first");
static_assert(rc::detail::HasShowValue<faker::rc::finance::Amount<faker::rc::finance::AmountSymOptions<>>>::value,
              "Make sure faker-rc/print.h is included first");
static_assert(rc::detail::HasShowValue<faker::rc::finance::Amount<faker::rc::finance::AmountRandomCurrency<>>>::value,
              "Make sure faker-rc/print.h is included first");
static_assert(rc::detail::HasShowValue<faker::rc::finance::Bic<>>::value,
              "Make sure faker-rc/print.h is included first");
static_assert(rc::detail::HasShowValue<faker::rc::finance::IncludeBranch>::value,
              "Make sure faker-rc/print.h is included first");
static_assert(rc::detail::HasShowValue<faker::rc::finance::MaskedNumber<>>::value,
              "Make sure faker-rc/print.h is included first");
static_assert(rc::detail::HasShowValue<faker::rc::finance::Pin<>>::value,
              "Make sure faker-rc/print.h is included first");
static_assert(rc::detail::HasShowValue<faker::rc::finance::Network>::value,
              "Make sure faker-rc/print.h is included first");
static_assert(rc::detail::HasShowValue<faker::rc::finance::CreditCard>::value,
              "Make sure faker-rc/print.h is included first");
static_assert(rc::detail::HasShowValue<faker::rc::finance::Currency>::value,
              "Make sure faker-rc/print.h is included first");


namespace rc {
    template<size_t MinLength, size_t MaxLength>
    struct Arbitrary<faker::rc::finance::AccountNumber<MinLength, MaxLength>> {
        static Gen<faker::rc::finance::AccountNumber<MinLength, MaxLength>> arbitrary() {
            return rc::gen::construct<faker::rc::finance::AccountNumber<MinLength, MaxLength>>(
                    rc::gen::mapcat(
                            rc::gen::inRange<size_t>(0, std::numeric_limits<size_t>::max()),
                            [](size_t index) {
                                auto mapFn = [index](size_t len) {
                                    return faker::rc::finance::AccountNumber<MinLength, MaxLength>{
                                            faker::finance::account_number(len, index)
                                    };
                                };
                                if constexpr (MinLength >= MaxLength) {
                                    return rc::gen::map(
                                            rc::gen::just(MinLength),
                                            mapFn
                                    );
                                } else {
                                    return rc::gen::map(
                                            rc::gen::inRange<size_t>(MinLength, MaxLength),
                                            mapFn
                                    );
                                }
                            }
                    )
            );
        }
    };

    template<faker::DataComplexity MaxComplexity>
    struct Arbitrary<faker::rc::finance::AccountName<MaxComplexity>> {
        static Gen<faker::rc::finance::AccountName<MaxComplexity>> arbitrary() {
            return rc::gen::construct<faker::rc::finance::AccountName<MaxComplexity>>(
                    faker::rc::generators::wrap_complex<decltype(faker::finance::account_name), MaxComplexity>(
                            faker::finance::account_name)
            );
        }
    };

    template<>
    struct Arbitrary<faker::rc::finance::TransactionType> {
        static Gen<faker::rc::finance::TransactionType> arbitrary() {
            return rc::gen::map(
                    rc::gen::inRange(0, static_cast<int>(faker::rc::finance::TransactionType::COUNT)),
                    [](int i) {
                        return static_cast<faker::rc::finance::TransactionType>(i);
                    }
            );
        }
    };

    template<>
    struct Arbitrary<faker::rc::finance::Network> {
        static Gen<faker::rc::finance::Network> arbitrary() {
            return rc::gen::map(
                    rc::gen::inRange(0, static_cast<int>(faker::rc::finance::Network::COUNT)),
                    [](int i) {
                        return static_cast<faker::rc::finance::Network>(i);
                    }
            );
        }
    };

    template<
            unsigned DecimalPlaces,
            int Max,
            int Min,
            faker::rc::finance::CurrencySymbolPos SymPos,
            char ...Symbols,
            char DecimalSeparator
    >
    struct Arbitrary<faker::rc::finance::Amount<
            faker::rc::finance::AmountSymOptions<
                    DecimalPlaces,
                    Max,
                    Min,
                    faker::rc::finance::Sym<SymPos, Symbols...>,
                    DecimalSeparator
            >
    >
    > {
        using Sym = faker::rc::finance::Sym<SymPos, Symbols...>;
        using Type = faker::rc::finance::Amount<
                faker::rc::finance::AmountSymOptions<
                        DecimalPlaces,
                        Max,
                        Min,
                        Sym,
                        DecimalSeparator
                >
        >;

        static Gen<Type> arbitrary() {
            return rc::gen::map(
                    rc::gen::inRange<size_t>(0, std::numeric_limits<size_t>::max()),
                    [](size_t index) {
                        return Type{faker::finance::amount(
                                faker::finance::AmountOptions{
                                        .decimalPlaces = DecimalPlaces,
                                        .max = Max,
                                        .min = Min,
                                        .symbol = Sym::str,
                                        .symbolPos = Sym::position,
                                        .decimalSeparator = std::string(1, DecimalSeparator),
                                },
                                index
                        )};
                    }
            );
        }
    };

    template<>
    struct Arbitrary<faker::rc::finance::CreditCard> {
        static Gen<faker::rc::finance::CreditCard> arbitrary() {
            return faker::rc::generators::wrap_index<decltype(faker::finance::credit_card::card)>(
                    faker::finance::credit_card::card);
        }
    };

    template<>
    struct Arbitrary<faker::rc::finance::Currency> {
        static Gen<faker::rc::finance::Currency> arbitrary() {
            return faker::rc::generators::wrap_index<decltype(faker::finance::currency)>(faker::finance::currency);
        }
    };

    template<
            unsigned DecimalPlaces,
            int Max,
            int Min,
            faker::rc::finance::CurrencySymbolPos SymPos,
            char DecimalSeparator
    >
    struct Arbitrary<faker::rc::finance::Amount<
            faker::rc::finance::AmountRandomCurrency<
                    DecimalPlaces,
                    Max,
                    Min,
                    SymPos,
                    DecimalSeparator
            >
    >
    > {
        using Type = faker::rc::finance::Amount<
                faker::rc::finance::AmountRandomCurrency<
                        DecimalPlaces,
                        Max,
                        Min,
                        SymPos,
                        DecimalSeparator
                >
        >;

        static Gen<Type> arbitrary() {
            return rc::gen::mapcat(
                    rc::gen::arbitrary<faker::rc::finance::Currency>(),
                    [](const faker::rc::finance::Currency &currency) {
                        return rc::gen::map(
                                rc::gen::inRange<size_t>(0, std::numeric_limits<size_t>::max()),
                                [=](size_t index) {
                                    return Type{faker::finance::amount(
                                            faker::finance::AmountOptions{
                                                    .decimalPlaces = DecimalPlaces,
                                                    .max = Max,
                                                    .min = Min,
                                                    .symbol = currency.symbol,
                                                    .symbolPos = SymPos,
                                                    .decimalSeparator = std::string(1, DecimalSeparator),
                                            },
                                            index
                                    )};
                                }
                        );
                    }
            );
        }
    };

    template<>
    struct Arbitrary<faker::rc::finance::Bic<faker::rc::finance::IncludeBranch::ALWAYS>> {
        static Gen<faker::rc::finance::Bic<faker::rc::finance::IncludeBranch::ALWAYS>> arbitrary() {
            return faker::rc::generators::wrap_index([](size_t index) {
                return faker::rc::finance::Bic<faker::rc::finance::IncludeBranch::ALWAYS>{
                        faker::finance::bic({.includeBranchCode = true}, index)
                };
            });
        }
    };

    template<>
    struct Arbitrary<faker::rc::finance::Bic<faker::rc::finance::IncludeBranch::NEVER>> {
        static Gen<faker::rc::finance::Bic<faker::rc::finance::IncludeBranch::NEVER>> arbitrary() {
            return faker::rc::generators::wrap_index([](size_t index) {
                return faker::rc::finance::Bic<faker::rc::finance::IncludeBranch::NEVER>{
                        faker::finance::bic({.includeBranchCode = false}, index)
                };
            });
        }
    };

    template<>
    struct Arbitrary<faker::rc::finance::Bic<faker::rc::finance::IncludeBranch::SOMETIMES>> {
        static Gen<faker::rc::finance::Bic<faker::rc::finance::IncludeBranch::SOMETIMES>> arbitrary() {
            return gen::mapcat(
                    gen::arbitrary<bool>(),
                    [](bool branchCode) {
                        return faker::rc::generators::wrap_index([=](size_t index) {
                            return faker::rc::finance::Bic<faker::rc::finance::IncludeBranch::SOMETIMES>{
                                    faker::finance::bic({.includeBranchCode = branchCode}, index)
                            };
                        });
                    }
            );
        }
    };

    template<size_t UnmaskedLen, size_t MaskedLen, char MaskChar, bool SurroundWithParens>
    struct Arbitrary<
            faker::rc::finance::MaskedNumber<
                    UnmaskedLen,
                    MaskedLen,
                    MaskChar,
                    SurroundWithParens
            >
    > {
        using Type = faker::rc::finance::MaskedNumber<
                UnmaskedLen,
                MaskedLen,
                MaskChar,
                SurroundWithParens
        >;

        static Gen<Type> arbitrary() {
            return faker::rc::generators::wrap_index(
                    [=](size_t index) {
                        return Type{
                                faker::finance::masked_number(
                                        {
                                                .unMaskedLength = UnmaskedLen,
                                                .maskedLength = MaskedLen,
                                                .maskedChar = MaskChar,
                                                .surroundWithParens = SurroundWithParens,
                                        },
                                        index
                                )
                        };
                    }
            );
        }
    };


    template<size_t MinLen, size_t MaxLen>
    struct Arbitrary<faker::rc::finance::Pin<MinLen, MaxLen>> {
        using Type = faker::rc::finance::Pin<MinLen, MaxLen>;

        static Gen<Type> arbitrary() {
            if constexpr (MinLen >= MaxLen) {
                return faker::rc::generators::wrap_index(
                        [=](size_t index) {
                            return Type{
                                    faker::finance::pin(
                                            {.length = MinLen},
                                            index
                                    )
                            };
                        }
                );
            }
            else {
                return gen::mapcat(
                        gen::inRange(MinLen, MaxLen),
                        [](size_t len) {
                            return faker::rc::generators::wrap_index(
                                    [=](size_t index) {
                                        return Type{
                                                faker::finance::pin(
                                                        {.length = len},
                                                        index
                                                )
                                        };
                                    }
                            );
                        }
                );
            }
        }
    };
}
