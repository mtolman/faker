#pragma once

#include <faker/malicious.h>
#include "../data/malicious.h"
#include "common-generators.h"
#include <rapidcheck.h>

static_assert(rc::detail::HasShowValue<faker::rc::malicious::SqlInjectionAttack>::value,
              "Make sure faker-rc/print.h is included first");
static_assert(rc::detail::HasShowValue<faker::rc::malicious::XssAttack>::value,
              "Make sure faker-rc/print.h is included first");
static_assert(rc::detail::HasShowValue<faker::rc::malicious::FormatInjectionAttack>::value,
              "Make sure faker-rc/print.h is included first");
static_assert(rc::detail::HasShowValue<faker::rc::malicious::EdgeCase>::value,
              "Make sure faker-rc/print.h is included first");


namespace rc {
    template<>
    struct Arbitrary<faker::rc::malicious::SqlInjectionAttack> {
        static Gen<faker::rc::malicious::SqlInjectionAttack> arbitrary() {
            return faker::rc::generators::wrap_index(
                    [](size_t index) {
                        return faker::rc::malicious::SqlInjectionAttack{
                                faker::malicious::sql_injection(index)
                        };
                    }
            );
        }
    };

    template<>
    struct Arbitrary<faker::rc::malicious::XssAttack> {
        static Gen<faker::rc::malicious::XssAttack> arbitrary() {
            return faker::rc::generators::wrap_index(
                    [](size_t index) {
                        return faker::rc::malicious::XssAttack{
                                faker::malicious::sql_injection(index)
                        };
                    }
            );
        }
    };

    template<>
    struct Arbitrary<faker::rc::malicious::FormatInjectionAttack> {
        static Gen<faker::rc::malicious::FormatInjectionAttack> arbitrary() {
            return faker::rc::generators::wrap_index(
                    [](size_t index) {
                        return faker::rc::malicious::FormatInjectionAttack{
                                faker::malicious::sql_injection(index)
                        };
                    }
            );
        }
    };

    template<>
    struct Arbitrary<faker::rc::malicious::EdgeCase> {
        static Gen<faker::rc::malicious::EdgeCase> arbitrary() {
            return faker::rc::generators::wrap_index(
                    [](size_t index) {
                        return faker::rc::malicious::EdgeCase{
                                faker::malicious::sql_injection(index)
                        };
                    }
            );
        }
    };
}