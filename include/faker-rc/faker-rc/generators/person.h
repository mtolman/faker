#pragma once

#include <faker/person.h>
#include <rapidcheck.h>
#include "common-generators.h"

static_assert(rc::detail::HasShowValue<::faker::person::Sex>::value, "Make sure faker-rc/print.h is included first!");
static_assert(rc::detail::HasShowValue<::faker::person::Person>::value, "Make sure faker-rc/print.h is included first!");
static_assert(rc::detail::HasShowValue<::faker::rc::person::Person<faker::DataComplexity::COMPLEX>>::value, "Make sure faker-rc/print.h is included first!");

namespace rc {
    template<>
    struct Arbitrary<faker::person::Sex> {
        static Gen<faker::person::Sex> arbitrary() {
            return ::rc::gen::construct<faker::person::Sex>(
                    gen::map(
                            gen::inRange(0, static_cast<int>(faker::person::Sex::COUNT)),
                            [](int d) {
                                return static_cast<faker::person::Sex>(d);
                            }
                    )
            );
        }
    };
}

namespace faker::rc::generators::person {
    template<typename F, faker::DataComplexity MaxComplexity = DataComplexity::COUNT>
    auto wrap_person_method(const F& func) -> ::rc::Gen<return_type_t<F>> {
        namespace gen = ::rc::gen;
        return gen::mapcat(
                maxed_complexity<MaxComplexity>(),
                [=, &func](faker::DataComplexity d) {
                    return gen::mapcat(
                            gen::arbitrary<faker::person::Sex>(),
                            [=, &func](faker::person::Sex sex) {
                                return gen::map(
                                        gen::inRange<size_t>(0, std::numeric_limits<size_t>::max()),
                                        [=](size_t index) {
                                            return func(d, sex, index);
                                        }
                                );
                            }
                    );
                }
        );
    }
}

namespace rc {
    template<>
    struct Arbitrary<faker::person::Person> {
        static Gen<faker::person::Person> arbitrary() {
            return ::rc::gen::construct<faker::person::Person>(
                    faker::rc::generators::person::wrap_person_method(faker::person::person)
            );
        }
    };

    template<faker::DataComplexity MaxComplexity>
    struct Arbitrary<faker::rc::person::Person<MaxComplexity>> {
        static Gen<faker::rc::person::Person<MaxComplexity>> arbitrary() {
            return ::rc::gen::construct<faker::rc::person::Person<MaxComplexity>>(
                    faker::rc::generators::person::wrap_person_method<decltype(faker::person::person), MaxComplexity>(faker::person::person)
            );
        }
    };

    template<faker::DataComplexity MaxComplexity>
    struct Arbitrary<faker::rc::person::GivenName<MaxComplexity>> {
        static Gen<faker::rc::person::GivenName<MaxComplexity>> arbitrary() {
            return ::rc::gen::construct<faker::rc::person::GivenName<MaxComplexity>>(
                    faker::rc::generators::person::wrap_person_method<decltype(faker::person::given_name), MaxComplexity>(faker::person::given_name)
            );
        }
    };

    template<faker::DataComplexity MaxComplexity>
    struct Arbitrary<faker::rc::person::Surname<MaxComplexity>> {
        static Gen<faker::rc::person::Surname<MaxComplexity>> arbitrary() {
            return ::rc::gen::construct<faker::rc::person::Surname<MaxComplexity>>(
                    faker::rc::generators::wrap_complex<decltype(faker::person::surname), MaxComplexity>(faker::person::surname)
            );
        }
    };

    template<faker::DataComplexity MaxComplexity>
    struct Arbitrary<faker::rc::person::FullName<MaxComplexity>> {
        static Gen<faker::rc::person::FullName<MaxComplexity>> arbitrary() {
            return ::rc::gen::construct<faker::rc::person::FullName<MaxComplexity>>(
                    faker::rc::generators::person::wrap_person_method<decltype(faker::person::full_name), MaxComplexity>(faker::person::full_name)
            );
        }
    };

    template<faker::DataComplexity MaxComplexity>
    struct Arbitrary<faker::rc::person::Suffix<MaxComplexity>> {
        static Gen<faker::rc::person::Suffix<MaxComplexity>> arbitrary() {
            return ::rc::gen::construct<faker::rc::person::Suffix<MaxComplexity>>(
                    faker::rc::generators::person::wrap_person_method<decltype(faker::person::suffix), MaxComplexity>(faker::person::suffix)
            );
        }
    };

    template<faker::DataComplexity MaxComplexity>
    struct Arbitrary<faker::rc::person::Prefix<MaxComplexity>> {
        static Gen<faker::rc::person::Prefix<MaxComplexity>> arbitrary() {
            return ::rc::gen::construct<faker::rc::person::Prefix<MaxComplexity>>(
                    faker::rc::generators::person::wrap_person_method<decltype(faker::person::prefix), MaxComplexity>(faker::person::prefix)
            );
        }
    };
}
