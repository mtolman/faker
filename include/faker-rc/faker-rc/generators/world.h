#pragma once

#include <faker/world.h>
#include "../data/world.h"
#include "common-generators.h"
#include <rapidcheck.h>

static_assert(rc::detail::HasShowValue<faker::rc::world::Country>::value,
              "Make sure faker-rc/print.h is included first");


namespace rc {
    template<>
    struct Arbitrary<faker::rc::world::Country> {
        static Gen<faker::rc::world::Country> arbitrary() {
            return faker::rc::generators::wrap_index(
                    [](size_t index) {
                        return faker::rc::world::Country{
                                faker::world::country(index)
                        };
                    }
            );
        }
    };
}
