#pragma once

#include <functional>
#include <optional>
#include <faker/common.h>
#include <rapidcheck.h>

static_assert(rc::detail::HasShowValue<::faker::DataComplexity>::value, "Make sure faker-rc/print.h is included first!");

namespace faker::rc::generators {
    template<typename F>
    using return_type_t = typename decltype(std::function{std::declval<F>()})::result_type;

    template<faker::DataComplexity MaxComplexity>
    auto maxed_complexity() -> ::rc::Gen<faker::DataComplexity> {
        if constexpr (MaxComplexity == faker::DataComplexity::RUDIMENTARY) {
            return ::rc::gen::just(faker::DataComplexity::RUDIMENTARY);
        }
        else {
            const auto max = std::min(static_cast<int>(MaxComplexity), static_cast<int>(faker::DataComplexity::COUNT));
            return ::rc::gen::map(
                    ::rc::gen::inRange(0, max),
                    [](int d) {
                        return static_cast<faker::DataComplexity>(d);
                    }
            );
        }
    }

    template<typename F, faker::DataComplexity MaxComplexity = faker::DataComplexity::COUNT>
    auto wrap_complex(const F& func) -> ::rc::Gen<return_type_t<F>> {
        namespace gen = ::rc::gen;
        return gen::mapcat(
                maxed_complexity<MaxComplexity>(),
                [=, &func](faker::DataComplexity d) {
                    return gen::map(
                            gen::inRange<size_t>(0, std::numeric_limits<size_t>::max()),
                            [=](size_t index) {
                                return func(d, index);
                            }
                    );
                }
        );
    }

    template<typename F>
    auto wrap_index(const F& func) -> ::rc::Gen<return_type_t<F>> {
        namespace gen = ::rc::gen;
        return  gen::map(
                gen::inRange<size_t>(0, std::numeric_limits<size_t>::max()),
                [=](size_t index) {
                    return func(index);
                }
        );
    }
}
