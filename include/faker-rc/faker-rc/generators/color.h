#pragma once

#include "../data.h"
#include <rapidcheck.h>

static_assert(rc::detail::HasShowValue<faker::rc::color::RGB>::value,
              "Make sure faker-rc/print.h is included first!");

static_assert(rc::detail::HasShowValue<faker::rc::color::RGBA>::value,
              "Make sure faker-rc/print.h is included first!");

static_assert(rc::detail::HasShowValue<faker::rc::color::HSL>::value,
              "Make sure faker-rc/print.h is included first!");

static_assert(rc::detail::HasShowValue<faker::rc::color::HSLA>::value,
              "Make sure faker-rc/print.h is included first!");

namespace rc {
    template<>
    struct Arbitrary<faker::rc::color::RGB> {
        static Gen<faker::rc::color::RGB> arbitrary() {
            return faker::rc::generators::wrap_index(faker::color::rgb);
        }
    };

    template<>
    struct Arbitrary<faker::rc::color::RGBA> {
        static Gen<faker::rc::color::RGBA> arbitrary() {
            return faker::rc::generators::wrap_index(faker::color::rgba);
        }
    };

    template<>
    struct Arbitrary<faker::rc::color::HSL> {
        static Gen<faker::rc::color::HSL> arbitrary() {
            return faker::rc::generators::wrap_index(faker::color::hsl);
        }
    };

    template<>
    struct Arbitrary<faker::rc::color::HSLA> {
        static Gen<faker::rc::color::HSLA> arbitrary() {
            return faker::rc::generators::wrap_index(faker::color::hsla);
        }
    };
}
