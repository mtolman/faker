#pragma once

#include "../data/ascii.h"
#include "common-generators.h"
#include <rapidcheck.h>

static_assert(rc::detail::HasShowValue<faker::rc::ascii::Numeric<4>>::value,
              "Make sure faker-rc/print.h is included first");
static_assert(rc::detail::HasShowValue<faker::rc::ascii::Alpha<4>>::value,
              "Make sure faker-rc/print.h is included first");
static_assert(rc::detail::HasShowValue<faker::rc::ascii::AlphaNumeric<4>>::value,
              "Make sure faker-rc/print.h is included first");
static_assert(rc::detail::HasShowValue<faker::rc::ascii::Hex<4>>::value,
              "Make sure faker-rc/print.h is included first");

namespace rc {
    template<size_t Length, bool AllowLeadingZeros, char... Excluded>
    struct Arbitrary<faker::rc::ascii::Numeric<Length, AllowLeadingZeros, Excluded...>> {
        static Gen<faker::rc::ascii::Numeric<Length, AllowLeadingZeros, Excluded...>> arbitrary() {
            return ::rc::gen::construct<faker::rc::ascii::Numeric<Length, AllowLeadingZeros, Excluded...>>(
                    rc::gen::map(
                            rc::gen::inRange<size_t>(0, std::numeric_limits<size_t>::max()),
                            [](size_t index) {
                                if constexpr (sizeof...(Excluded) > 0) {
                                    return faker::ascii::numeric({
                                                                         .length = Length,
                                                                         .allowLeadingZeros = AllowLeadingZeros,
                                                                         .exclude = std::set<char>{Excluded...}
                                                                 }, index);
                                } else {
                                    return faker::ascii::numeric({
                                                                         .length = Length,
                                                                         .allowLeadingZeros = AllowLeadingZeros,
                                                                 }, index);
                                }
                            }
                    )
            );
        }
    };

    template<size_t Length, faker::ascii::Casing Casing, char... Excluded>
    struct Arbitrary<faker::rc::ascii::Alpha<Length, Casing, Excluded...>> {
        static Gen<faker::rc::ascii::Alpha<Length, Casing, Excluded...>> arbitrary() {
            return ::rc::gen::construct<faker::rc::ascii::Alpha<Length, Casing, Excluded...>>(
                    rc::gen::map(
                            rc::gen::inRange<size_t>(0, std::numeric_limits<size_t>::max()),
                            [](size_t index) {
                                if constexpr (sizeof...(Excluded) > 0) {
                                    return faker::ascii::alpha({
                                                                       .length = Length,
                                                                       .casing = Casing,
                                                                       .exclude = std::set<char>{Excluded...}
                                                               }, index);
                                } else {
                                    return faker::ascii::alpha({
                                                                       .length = Length,
                                                                       .casing = Casing,
                                                               }, index);
                                }
                            }
                    )
            );
        }
    };

    template<size_t Length, faker::ascii::Casing Casing, char... Excluded>
    struct Arbitrary<faker::rc::ascii::AlphaNumeric<Length, Casing, Excluded...>> {
        static Gen<faker::rc::ascii::AlphaNumeric<Length, Casing, Excluded...>> arbitrary() {
            return ::rc::gen::construct<faker::rc::ascii::AlphaNumeric<Length, Casing, Excluded...>>(
                    rc::gen::map(
                            rc::gen::inRange<size_t>(0, std::numeric_limits<size_t>::max()),
                            [](size_t index) {
                                if constexpr (sizeof...(Excluded) > 0) {
                                    return faker::ascii::alpha_num({
                                                                           .length = Length,
                                                                           .casing = Casing,
                                                                           .exclude = std::set<char>{Excluded...}
                                                                   }, index);
                                } else {
                                    return faker::ascii::alpha_num({
                                                                           .length = Length,
                                                                           .casing = Casing,
                                                                   }, index);
                                }
                            }
                    )
            );
        }
    };

    template<size_t Length, faker::ascii::Casing Casing>
    struct Arbitrary<faker::rc::ascii::Hex<Length, Casing>> {
        static Gen<faker::rc::ascii::Hex<Length, Casing>> arbitrary() {
            return ::rc::gen::construct<faker::rc::ascii::Hex<Length, Casing>>(
                    rc::gen::map(
                            rc::gen::inRange<size_t>(0, std::numeric_limits<size_t>::max()),
                            [](size_t index) {
                                return faker::ascii::hex({
                                                                 .length = Length,
                                                                 .casing = Casing
                                                         }, index);
                            }
                    )
            );
        }
    };
}