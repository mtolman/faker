#pragma once

#include <faker/finance.h>
#include <array>

/**
 * Finance property test module
 */
namespace faker::rc::finance {
    /**
     * Property test parameter for an account number text
     * @tparam MinLength Minimum length of account number
     * @tparam MaxLength Maximum length of account number
     */
    template<size_t MinLength, size_t MaxLength = MinLength>
    struct AccountNumber {
        std::string value;
    };

    /**
     * Property test parameter for account name
     * @tparam MaxComplexity Maximum complexity to allow for output
     */
    template<faker::DataComplexity MaxComplexity = faker::DataComplexity::COUNT>
    struct AccountName {
        std::string value;
    };

    /**
     * Property test parameter for transaction type enum
     */
    using TransactionType = faker::finance::TransactionType;

    /**
     * Property test parameter for currency symbol position enum
     */
    using CurrencySymbolPos = faker::finance::CurrencySymbolPos;

    /**
     * Holds compile-time symbol information
     * @tparam Pos Position for the symbol
     * @tparam Symbol Characters that compose the symbol
     */
    template<CurrencySymbolPos Pos = faker::finance::CurrencySymbolPos::PREFIX, char ...Symbol>
    struct Sym {
        static constexpr bool exists = sizeof...(Symbol) > 0;
        static constexpr CurrencySymbolPos position = Pos;
        static constexpr auto chars = std::array{Symbol..., '\0'};
        static constexpr const char* str = chars.data();
    };

    /**
     * Amount options options where the symbol is manually defined
     * @tparam DecimalPlaces Number of decimal places for the amount
     * @tparam Max Maximum amount value
     * @tparam Min Minimum amount value
     * @tparam Symbol Symbol (@see Sym)
     * @tparam DecimalSeparator Decimal separator
     */
    template<
        unsigned DecimalPlaces = 2,
        int Max = 1000000,
        int Min = 0,
        typename Symbol = Sym<finance::CurrencySymbolPos::PREFIX>,
        char DecimalSeparator = '.'
    >
    struct AmountSymOptions{
        static constexpr auto decPlaces = DecimalPlaces;
        static constexpr char decSeparator = '.';
        static constexpr double max = Max;
        static constexpr double min = Min;
        using sym = Symbol;
    };

    /**
     * Amount options where the symbol is from a random currency
     * @tparam DecimalPlaces Number of decimal places for the amount
     * @tparam Max Maximum amount value
     * @tparam Min Minimum amount value
     * @tparam Pos Position of the symbol
     * @tparam DecimalSeparator Decimal separator
     */
    template<
            unsigned DecimalPlaces = 2,
            int Max = 1000000,
            int Min = 0,
            finance::CurrencySymbolPos Pos = finance::CurrencySymbolPos::PREFIX,
            char DecimalSeparator = '.'
    >
    struct AmountRandomCurrency{
        static constexpr auto decPlaces = DecimalPlaces;
        static constexpr char decSeparator = '.';
        static constexpr double max = Max;
        static constexpr double min = Min;
        static constexpr finance::CurrencySymbolPos pos = Pos;
    };

    /**
     * Property test parameter for monetary amounts
     * @tparam Options Amount options (@see AmountSymOptions, @see AmountRandomCurrency)
     */
    template<typename Options = AmountSymOptions<>>
    struct Amount {
        using opts = Options;
        std::string value;
    };

    /**
     * Enum for when to include branch codes for BIC property test generation
     */
    enum class IncludeBranch {
        ALWAYS,
        NEVER,
        SOMETIMES
    };

    /**
     * Property test parameter for Bic
     * @tparam IncludeBranchCode When to include branch codes
     */
    template<IncludeBranch IncludeBranchCode = IncludeBranch::SOMETIMES>
    struct Bic {
        std::string value;
    };

    /**
     * Property test parameter for masked numbers
     * @tparam UnmaskedLen Length of unmasked number
     * @tparam MaskedLen Length of masked number
     * @tparam MaskChar Character to use for masking
     * @tparam SurroundWithParens Whether to surround with parenthesis
     */
    template<
            size_t UnmaskedLen = 4,
            size_t MaskedLen = 3,
            char MaskChar = '.',
            bool SurroundWithParens = true
    >
    struct MaskedNumber {
        std::string value;
    };

    /**
     * Property test parameter for PINs
     * @tparam MinLen Minimum pin length
     * @tparam MaxLen Maximum pin length
     */
    template<
            size_t MinLen = 4,
            size_t MaxLen = MinLen
    >
    struct Pin {
        std::string value;
    };

    /**
     * Property test parameter for credit card network enum
     */
    using Network = faker::finance::credit_card::Network;

    /**
     * Property test parameter for credit cards
     */
    using CreditCard = faker::finance::credit_card::Card;

    /**
     * Property test parameter for currency
     */
    using Currency = faker::finance::Currency;
}
