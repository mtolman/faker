#pragma once

#include <faker/person.h>

/**
 * Person property test module
 */
namespace faker::rc::person {
    /**
     * Property test parameter for Person
     * @tparam MaxComplexity Maximum complexity for generated output
     */
    template<faker::DataComplexity MaxComplexity = faker::DataComplexity::COUNT>
    struct Person {
        ::faker::person::Person value;

        auto operator->() const -> const ::faker::person::Person* { return &value; }
        auto operator*() const -> const ::faker::person::Person& { return value; }
    };

    /**
     * Property test parameter for name prefixes
     * @tparam MaxComplexity Maximum complexity for generated output
     */
    template<faker::DataComplexity MaxComplexity = faker::DataComplexity::COUNT>
    struct Prefix {
        std::optional<std::string> value;
        friend std::ostream& operator<<(std::ostream& os, const Prefix& d) {
            os << d.value.value_or("<none>");
            return os;
        }
    };

    /**
     * Property test parameter for name suffixes
     * @tparam MaxComplexity Maximum complexity for generated output
     */
    template<faker::DataComplexity MaxComplexity = faker::DataComplexity::COUNT>
    struct Suffix {
        std::optional<std::string> value;
        friend std::ostream& operator<<(std::ostream& os, const Suffix& d) {
            os << d.value.value_or("<none>");
            return os;
        }
    };

    /**
     * Property test parameter for given name
     * @tparam MaxComplexity Maximum complexity for generated output
     */
    template<faker::DataComplexity MaxComplexity = faker::DataComplexity::COUNT>
    struct GivenName {
        std::string value;
        friend std::ostream& operator<<(std::ostream& os, const GivenName& d) {
            os << d.value;
            return os;
        }
    };

    /**
     * Property test parameter for surname
     * @tparam MaxComplexity Maximum complexity for generated output
     */
    template<faker::DataComplexity MaxComplexity = faker::DataComplexity::COUNT>
    struct Surname {
        std::string value;

        friend std::ostream& operator<<(std::ostream& os, const Surname& d) {
            os << d.value;
            return os;
        }
    };

    /**
     * Property test parameter for full name
     * @tparam MaxComplexity Maximum complexity for generated output
     */
    template<faker::DataComplexity MaxComplexity = faker::DataComplexity::COUNT>
    struct FullName {
        std::string value;

        friend std::ostream& operator<<(std::ostream& os, const FullName& d) {
            os << d.value;
            return os;
        }
    };
}
