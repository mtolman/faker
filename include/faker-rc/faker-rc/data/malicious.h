#pragma once

#include <string>

/**
 * Malicious property test module
 */
namespace faker::rc::malicious {
    /**
     * Property test parameter for SQL Injection
     */
    struct SqlInjectionAttack {
        std::string value;
    };

    /**
     * Property test parameter for XSS
     */
    struct XssAttack {
        std::string value;
    };

    /**
     * Property test parameter for Format Injection
     */
    struct FormatInjectionAttack {
        std::string value;
    };

    /**
     * Property test parameter for Edge Case
     */
    struct EdgeCase {
        std::string value;
    };
}
