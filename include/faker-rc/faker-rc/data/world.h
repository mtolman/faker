#pragma once

#include <string>
#include <faker/world.h>

/**
 * World property module
 */
namespace faker::rc::world {
    /**
     * Property test parameter for Country
     */
    using Country = faker::world::Country;
}
