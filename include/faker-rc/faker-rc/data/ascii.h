#pragma once

#include <faker/ascii.h>

/**
 * Module for generating ASCII data as part of property tests
 */
namespace faker::rc::ascii {
    /**
     * Property test parameter for numeric ASCII text
     * @tparam Length Length of the numeric text
     * @tparam AllowLeadingZeros Whether to allow leading zeroes
     * @tparam Excluded Characters to exclude from generated output
     */
    template<size_t Length, bool AllowLeadingZeros = true, char... Excluded>
    struct Numeric {
        std::string value;
    };

    /**
     * Property test parameter for alphabetic ASCII text
     * @tparam Length Length of the text
     * @tparam Casing Casing (MiXeD, UPPER, lower)
     * @tparam Excluded Characters to exclude from the output
     */
    template<size_t Length, faker::ascii::Casing Casing = faker::ascii::Casing::MIXED, char... Excluded>
    struct Alpha {
        std::string value;
    };

    /**
     * Property test parameter for AlphaNumeric ASCII text
     * @tparam Length Length of the text
     * @tparam Casing Casing (MiXeD, UPPER, lower)
     * @tparam Excluded Characters to exclude from the output
     */
    template<size_t Length, faker::ascii::Casing Casing = faker::ascii::Casing::MIXED, char... Excluded>
    struct AlphaNumeric {
        std::string value;
    };

    /**
     * Property test parameter for hexadecimal ASCII text
     * @tparam Length Length of the text
     * @tparam Casing Casing (MiXeD, UPPER, lower)
     * @tparam Excluded Characters to exclude from the output
     */
    template<size_t Length, faker::ascii::Casing Casing = faker::ascii::Casing::MIXED>
    struct Hex {
        std::string value;
    };
}
