#pragma once

#include <faker/color.h>

namespace faker::rc::color {
    using RGB = faker::color::RGB;

    using RGBA = faker::color::RGBA;

    using HSL = faker::color::HSL;

    using HSLA = faker::color::HSLA;
}
