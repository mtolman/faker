#pragma once

#include "data.h"

#include "print/common.h"
#include "print/ascii.h"
#include "print/color.h"
#include "print/finance.h"
#include "print/malicious.h"
#include "print/person.h"
#include "print/world.h"
