#pragma once

#include <string>
#include <optional>

/**
 * Module for print about the world
 */
namespace faker::world {
    /**
     * Data for a country. Based off of ISO print from the CIA
     */
    struct Country {
        /* Name of the country */
        std::string name;
        /* ISO Alpha-3 code */
        std::string alpha3;
        /* ISO Alpha-2 code */
        std::string alpha2;
        /* ISO Numeric code */
        std::string numericCode;
        /* Top level domain */
        std::optional<std::string> domain;
    };

    /**
     * Returns a random country
     * @param index Deterministic index
     * @return
     */
    auto country(std::optional<size_t> index = std::nullopt) -> Country;

    /**
     * Types of ISO country codes
     */
    enum class CountryCodeType {
        ALPHA2,
        ALPHA3,
        NUMERIC
    };

    /**
     * Options for getting a country code
     */
    struct CountryCodeOpts {
        CountryCodeType type = CountryCodeType::ALPHA3;
    };

    /**
     * Gets a country code
     * @param opts Options
     * @param index Deterministic index
     * @return
     */
    inline auto country_code(const CountryCodeOpts& opts, std::optional<size_t> index = std::nullopt) -> std::string {
        switch(opts.type) {
            case CountryCodeType::ALPHA2:
                return country(index).alpha2;
            case CountryCodeType::ALPHA3:
                return country(index).alpha3;
            case CountryCodeType::NUMERIC:
                return country(index).numericCode;
        }
    }

    /**
     * Gets a country name
     * @param index Deterministic index
     * @return
     */
    inline auto country_name(std::optional<size_t> index = std::nullopt) -> std::string { return country(index).name; }

    /**
     * Gets a country's top-level domain
     * @param index Deterministic index
     * @return
     */
    inline auto country_domain(std::optional<size_t> index = std::nullopt) -> std::optional<std::string> { return country(index).domain; }
}
