#pragma once

#include <string>
#include <optional>
#include <faker/common.h>

/**
 * Finance module for generating fake financial print
 */
namespace faker::finance {
    /**
     * Creates an account number of a given length (e.g. Bank account number)
     * @param length Length of the account number
     * @param index Deterministic index
     * @return
     */
    auto account_number(
            size_t length,
            std::optional<size_t> index = std::nullopt
    ) -> std::string;

    /**
     * Creates an account name
     * @param complexity Data complexity level
     * @param index Deterministic index
     * @return
     */
    auto account_name(
            std::optional<faker::DataComplexity> complexity = std::nullopt,
            std::optional<size_t> index = std::nullopt
    ) -> std::string;

    // TODO: Do transaction description
//    auto transaction_description(
//            size_t length,
//            std::optional<faker::DataComplexity> complexity = std::nullopt,
//            std::optional<size_t> index = std::nullopt
//    ) -> std::string;

    /**
     * Represents the type of a transaction
     */
    enum class TransactionType {
        DEPOSIT,
        WITHDRAWAL,
        PAYMENT,
        INVOICE,

        COUNT
    };

    /**
     * Returns a transaction type
     * @param index Deterministic index
     * @return
     */
    auto transaction_type(std::optional<size_t> index = std::nullopt) -> TransactionType;

    /**
     * Position for a currency symbol to appear
     */
    enum class CurrencySymbolPos {
        PREFIX,
        SUFFIX
    };

    /**
     * Creates text representing a monetary amount
     */
    struct AmountOptions {
        /** Number of decimal places to have in the amount */
        unsigned decimalPlaces = 2;
        /** Maximum value of the output */
        double max = 1000000;
        /** Minimum value of the output */
        double min = 0;
        /* Currency symbol to use. If empty, no symbol will be used */
        std::string symbol = {};
        /* Where to put the currency symbol */
        CurrencySymbolPos symbolPos = CurrencySymbolPos::PREFIX;
        /* The decimal separator to use */
        std::string decimalSeparator = ".";
    };

    /**
     * Generates text which represents a monetary amount
     * @param options Amount options
     * @param index Deterministic index
     * @return
     */
    auto amount(
            const AmountOptions& options,
            std::optional<size_t> index = std::nullopt
    ) -> std::string;

    /**
     * Generates text which represents a monetary amount. Uses default options
     * @param index Deterministic index
     * @return
     */
    inline auto amount(
            std::optional<size_t> index = std::nullopt
    ) -> std::string { return amount({}, index); }

    /**
     * Generates a BIC code
     */
    struct BicOptions {
        /** Whether to include a 3 character branch code */
        bool includeBranchCode = false;
    };

    /**
     * Creates a BIC code
     * @param options Bic options
     * @param index Deterministic index
     * @return
     */
    auto bic(BicOptions options, std::optional<size_t> index = std::nullopt) -> std::string;

    /**
     * Creates a BIC code with default options
     * @param index Deterministic index
     * @return
     */
    inline auto bic(std::optional<size_t> index = std::nullopt) -> std::string { return bic({}, index); }

    // TODO: Do iban stuff
//    struct IbanOptions {
//        std::optional<std::string> countryCode = std::nullopt;
//        bool formatted = false;
//    };
//    auto iban(IbanOptions options, std::optional<size_t> index = std::nullopt) -> std::string;
//    inline auto iban(std::optional<size_t> index = std::nullopt) -> std::string { return iban({}, index); }

    /**
     * Options for creating masked numbers (e.g. ####5644, ...4324)
     */
    struct MaskedNumberOptions {
        /** Length of the unmasked portion */
        size_t unMaskedLength = 4;
        /** Length of the masked portion */
        size_t maskedLength = 3;
        /** Character to use for masking */
        char maskedChar = '.';
        /** Whether to surround the text with parenthesis */
        bool surroundWithParens = true;
    };

    /**
     * Generates a masked number
     * @param options Options for masking
     * @param index Deterministic index
     * @return
     */
    auto masked_number(const MaskedNumberOptions& options, std::optional<size_t> index = std::nullopt) -> std::string;

    /**
     * Generates a masked number with default options
     * @param index Deterministic index
     * @return
     */
    inline auto masked_number(std::optional<size_t> index = std::nullopt) -> std::string { return masked_number({}, index); }

    /**
     * Options for generating a PIN
     */
    struct PinOptions {
        /** Lenght of the PIN */
        size_t length = 4;
    };

    /**
     * Generates a PIN
     * @param options PIN options
     * @param index Deterministic index
     * @return
     */
    auto pin(PinOptions options, std::optional<size_t> index = std::nullopt) -> std::string;

    /**
     * Generates a PIN with default options
     * @param index Deterministic index
     * @return
     */
    inline auto pin(std::optional<size_t> index = std::nullopt) -> std::string { return pin({}, index); }

    namespace credit_card {
        /**
         * List of networks supported by the library
         */
        enum class Network {
            AMERICAN_EXPRESS,
            DINERS,
            DISCOVER,
            ENROUTE,
            JCB15,
            JCB16,
            MASTERCARD,
            VOYAGER,
            VISA,

            COUNT
        };

        /**
         * Represents credit card information
         */
        struct Card {
            /** Card CVV (3 or 4 characters) */
            std::string cvv;
            /** Card Network */
            Network network;
            /** Card number */
            std::string number;
            /** Card expiration date (format MM/YYYY). May be in the past or future */
            std::string expiration;
        };

        /**
         * Generates a fake credit card
         * @param index Deterministic index
         * @return
         */
        auto card(std::optional<size_t> index = std::nullopt) -> Card;

        /**
         * Generates a fake credit card network
         * @param index Deterministic index
         * @return
         */
        auto network(std::optional<size_t> index = std::nullopt) -> Network;

        /**
         * Generates a fake credit card CVV
         * @param index Deterministic index
         * @return
         */
        auto cvv(std::optional<size_t> index = std::nullopt) -> std::string;

        /**
         * Generates a fake credit card number
         * @param index Deterministic index
         * @return
         */
        auto number(std::optional<size_t> index = std::nullopt) -> std::string;

        /**
         * Generates a fake credit card expiration date
         * @param index Deterministic index
         * @return
         */
        auto expiration(std::optional<size_t> index = std::nullopt) -> std::string;
    }

    /**
     * ISO Currency information
     */
    struct Currency {
        /** ISO currency code */
        const std::string code;
        /** ISO currency name */
        const std::string name;
        /** Symbol commonly used for the currency */
        const std::string symbol;
    };

    /**
     * Generates a random ISO currency
     * @param index Deterministic index
     * @return
     */
    auto currency(std::optional<size_t> index = std::nullopt) -> Currency;

    /**
     * Generates a random ISO currency code
     * @param index Deterministic index
     * @return
     */
    inline auto currency_code(std::optional<size_t> index = std::nullopt) -> std::string { return currency(index).code; }

    /**
     * Generates a random ISO currency name
     * @param index Deterministic index
     * @return
     */
    inline auto currency_name(std::optional<size_t> index = std::nullopt) -> std::string { return currency(index).name; }

    /**
     * Generates a random ISO currency symbol
     * @param index Deterministic index
     * @return
     */
    inline auto currency_symbol(std::optional<size_t> index = std::nullopt) -> std::string { return currency(index).symbol; }
}
