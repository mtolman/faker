#pragma once

#include <string>
#include <set>
#include <optional>

/**
 * @brief Module for generating random ASCII strings
 */
namespace faker::ascii {
    /**
     * Casing for alphabetical letters
     */
    enum class Casing {
        UPPER,
        LOWER,
        MIXED
    };

    /**
     * Options for generating numeric ASCII strings
     */
    struct NumericOpts {
        /** Length of the output */
        size_t length = 5;
        /** If true, it will allow the text to start with a '0' */
        bool allowLeadingZeros = true;
        /** List of ASCII characters to exclude */
        std::optional<std::set<char>> exclude = std::nullopt;
    };

    /**
     * Generates a numeric ASCII string (e.g. "12495834")
     * @param options Options for generation
     * @param index Deterministic index to use as generator basis
     * @return
     */
    auto numeric(const NumericOpts& options, std::optional<size_t> index = std::nullopt) -> std::string;

    /**
     * Options for generating alphabetical ASCII characters
     */
    struct AlphaOpts {
        /** Length of the output */
        size_t length = 3;
        /* Casing of the alphabetical text */
        Casing casing = Casing::MIXED;
        /* Characters to exclude (it is case-sensitive) */
        std::optional<std::set<char>> exclude = std::nullopt;
    };

    /**
     * Generates alphabetical ASCII text
     * @param options Options for generation
     * @param index Deterministic index to use as generator basis
     * @return
     */
    auto alpha(const AlphaOpts& opts, std::optional<size_t> index = std::nullopt) -> std::string;

    /**
     * Options for generating alphabetical and numeric ASCII characters
     */
    struct AlphaNumOpts {
        /** Length of the output */
        size_t length = 3;
        /* Casing of the alphabetical text */
        Casing casing = Casing::MIXED;
        /* Characters to exclude (it is case-sensitive) */
        std::optional<std::set<char>> exclude = std::nullopt;
    };

    /**
     * Generates AlphaNumeric ASCII text
     * @param options Options for generation
     * @param index Deterministic index to use as generator basis
     * @return
     */
    auto alpha_num(const AlphaNumOpts& opts, std::optional<size_t> index = std::nullopt) -> std::string;

    /**
     * Options for generating hexadecimal ASCII characters
     */
    struct HexOpts {
        /** Length of the output */
        size_t length = 3;
        /* Casing of the alphabetical text */
        Casing casing = Casing::MIXED;
    };

    /**
     * Generates Hexadecimal ASCII text
     * @param options Options for generation
     * @param index Deterministic index to use as generator basis
     * @return
     */
    inline auto hex(const HexOpts& opts, std::optional<size_t> index = std::nullopt) -> std::string {
        return alpha_num(AlphaNumOpts{.length = opts.length, .casing = opts.casing, .exclude = std::set{
                'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x','y', 'z',
                'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X','Y', 'Z',
        }});
    }
}
