#pragma once

namespace faker {
    /**
     * Data complexity levels
     * Generally used for text that's more "free form"
     */
    enum class DataComplexity {
        RUDIMENTARY,
        BASIC,
        INTERMEDIATE,
        ADVANCED,
        COMPLEX,

        COUNT
    };
}
