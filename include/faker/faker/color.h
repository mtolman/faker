#pragma once

#include <optional>
#include <array>
#include <string>

namespace faker::color {
    struct RGB {
        double red; double green; double blue;

        [[nodiscard]] auto css() const -> std::string;
        [[nodiscard]] auto bytes() const -> std::array<uint8_t, 3>;
        [[nodiscard]] auto hex(const std::string& prefix = "") const -> std::string;
    };
    auto rgb(std::optional<size_t> index = std::nullopt) -> RGB;

    struct RGBA {
        double red; double green; double blue; double alpha;
        [[nodiscard]] auto css() const -> std::string;
        [[nodiscard]] auto bytes() const -> std::array<uint8_t, 4>;
        [[nodiscard]] auto hex(const std::string& prefix = "") const -> std::string;
    };
    auto rgba(std::optional<size_t> index = std::nullopt) -> RGBA;

    struct HSL {
        double hue; double saturation; double luminosity;
        [[nodiscard]] auto css() const -> std::string;
    };
    auto hsl(std::optional<size_t> index = std::nullopt) -> HSL;

    struct HSLA {
        double hue; double saturation; double luminosity; double alpha;
        [[nodiscard]] auto css() const -> std::string;
    };
    auto hsla(std::optional<size_t> index = std::nullopt) -> HSLA;
}
