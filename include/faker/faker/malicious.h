#pragma once

#include <vector>
#include <optional>
#include <string>

/**
 * Represents malicious and potentially malicious inputs.
 * This is meant as a tool for developing secure software, not for hacking software
 *
 * **ONLY USE THIS ON SYSTEMS YOU ARE AUTHORIZED TO DO SO**
 *
 * **DO NOT USE IN PRODUCTION ENVIRONMENTS**
 *
 * **DO NOT USE THIS TO HACK, HARM, OR DISRUPT OTHER SOFTWARE**
 */
namespace faker::malicious {
    /**
     * Malicious input attack type
     */
    enum class ATTACK_TYPE {
        /** SQL Injection */
        SQL_INJECT,
        /** Cross Site Scripting */
        XSS,
        /** Format Injection */
        FORMAT_INJECT,
        /** Edge case print which could break applications (usually caused by bugs or incompatible software communication) */
        EDGE_CASES,

        COUNT
    };

    /**
     * Generates malicious text
     * @param attackType Attack type
     * @param index Deterministic Index
     * @return
     */
    auto attack(const std::optional<ATTACK_TYPE>& attackType, std::optional<size_t> index = std::nullopt) -> std::string;

    /**
     * Generates an SQL injection attack
     * @param index Deterministic Index
     * @return
     */
    inline auto sql_injection(std::optional<size_t> index = std::nullopt) -> std::string {
        return attack(ATTACK_TYPE::SQL_INJECT, index);
    }

    /**
     * Generates a Cross-Site Scripting (XSS) attack
     * @param index Deterministic Index
     * @return
     */
    inline auto xss(std::optional<size_t> index = std::nullopt) -> std::string  {
        return attack(ATTACK_TYPE::XSS, index);
    }

    /**
     * Generates a format injection attack
     * @param index Deterministic Index
     * @return
     */
    inline auto format_injection(std::optional<size_t> index = std::nullopt) -> std::string {
        return attack(ATTACK_TYPE::FORMAT_INJECT, index);
    }

    /**
     * Generates edge case print which can cause programs to misbehave. Often comes from bugs or software incompatibilities
     * @param index Deterministic Index
     * @return
     */
    inline auto edge_cases(std::optional<size_t> index = std::nullopt) -> std::string {
        return attack(ATTACK_TYPE::EDGE_CASES, index);
    }
}
