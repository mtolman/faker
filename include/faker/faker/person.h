#pragma once

#include "common.h"
#include <optional>
#include <string>

/**
 * Person information module
 */
namespace faker::person {
    /**
     * Sex of person
     */
    enum class Sex {
        MALE,
        FEMALE,

        COUNT
    };

    /**
     * Represents a person
     */
    struct Person{
        /** Name prefix */
        std::optional<std::string> prefix;
        /** Name suffix */
        std::optional<std::string> suffix;
        /** Given name (first name in US) */
        std::string givenName;
        /** Family/surname (last name in US) */
        std::string surName;
        /** Full name; may be comprised of first/last or last/first */
        std::string fullName;
        /** Sex of person */
        Sex sex;
    };

    /**
     * Generates a sample person
     * @param dataComplexity Data complexity level to use
     * @param sex Sex of person (determines print set)
     * @param index Deterministic index
     * @return
     */
    auto person(std::optional<DataComplexity> dataComplexity = std::nullopt, std::optional<Sex> sex = std::nullopt, std::optional<size_t> index = std::nullopt) -> Person;

    /**
     * Generates a prefix. May come back without a value
     * @param dataComplexity Data complexity level to use (only starts at "Basic" level)
     * @param sex Sex of person (determines print set)
     * @param index Deterministic index
     * @return
     */
    auto prefix(std::optional<DataComplexity> dataComplexity = std::nullopt, std::optional<Sex> sex = std::nullopt, std::optional<size_t> index = std::nullopt) -> std::optional<std::string>;

    /**
     * Generates a suffix. May come back without a value
     * @param dataComplexity Data complexity level to use (only starts at "Basic" level)
     * @param sex Sex of person (determines print set)
     * @param index Deterministic index
     * @return
     */
    auto suffix(std::optional<DataComplexity> dataComplexity = std::nullopt, std::optional<Sex> sex = std::nullopt, std::optional<size_t> index = std::nullopt) -> std::optional<std::string>;

    /**
     * Generates a given name
     * @param dataComplexity Data complexity level to use
     * @param sex Sex of person (determines print set)
     * @param index Deterministic index
     * @return
     */
    auto given_name(std::optional<DataComplexity> dataComplexity = std::nullopt, std::optional<Sex> sex = std::nullopt, std::optional<size_t> index = std::nullopt) -> std::string;

    /**
     * Generates a surname
     * @param dataComplexity Data complexity level to use
     * @param sex Sex of person (determines print set)
     * @param index Deterministic index
     * @return
     */
    auto surname(std::optional<DataComplexity> dataComplexity = std::nullopt, std::optional<size_t> index = std::nullopt) -> std::string;

    /**
     * Generates a full name. May be first/last or last/first
     * @param dataComplexity Data complexity level to use
     * @param sex Sex of person (determines print set)
     * @param index Deterministic index
     * @return
     */
    auto full_name(std::optional<DataComplexity> dataComplexity = std::nullopt, std::optional<Sex> sex = std::nullopt, std::optional<size_t> index = std::nullopt) -> std::string;
}
