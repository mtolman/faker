#pragma once

#include "faker/ascii.h"
#include "faker/common.h"
#include "faker/color.h"
#include "faker/finance.h"
#include "faker/malicious.h"
#include "faker/person.h"
#include "faker/world.h"
