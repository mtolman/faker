# Property Testing

Property testing is done with the faker-rc library (`faker::rc` for CMake). This module
integrates with [RapidCheck](https://github.com/emil-e/rapidcheck) for property
testing. The goal is to expose wrappers around the faker library to make usage of
property testing easier.

For faker-rc, it's important that files are included in a very specific way. This is
due to a limitation with RapidCheck's SFINAE. If the includes are not done in the
correct order, then the classes from faker will not get proper debug printing.

The order needs to be `faker-rc.h` followed by `rapidcheck.h`. The `faker-rc.h` will
take care of including faker-rc headers in the correct order.

## Adding Property Tests

All modules are submodules of `faker::rc` (e.g. `faker::rc::ascii`). To add a faker
object, just add a parameter of the proper type to your `rc::check` callback.

Some examples:

```cpp
// Inject a numeric ASCII string
rc::check([](const faker::rc::ascii::Numeric<5> &str) {
    RC_ASSERT(str.value.length() == 5);
    
    // Run a check with the numeric string
    RC_ASSERT(my_code(str) == 4);
});
```

```cpp
// Injects an XSS attack
rc::check([](const faker::rc::malicious::XssAttack &attack) {
    RC_ASSERT(!attack.value.empty());
    
    RC_ASSERT(is_safe(sanitize_sql(attack)));
});
```

### Passing Parameters

Sometimes extra parameters are needed. These parameters are passed through template
parameters so that they will be usable by RapidCheck. If a parameter isn't strictly
needed, it will be defaulted. Below are some examples

```cpp
// Injects a person with default parameters
rc::check([](const faker::rc::person::Person<> &person) {
    RC_ASSERT(is_valid_user(person.value));
});

// Injects a person with a maximum data complexity level of "BASIC"
rc::check([](const faker::rc::person::Person<faker::DataComplexity::BASIC> &person) {
    RC_ASSERT(is_valid_user(person.value));
});

// Injects a person with a maximum data complexity level of "BASIC"
rc::check([](const faker::rc::person::Person<faker::DataComplexity::BASIC> &person) {
RC_ASSERT(is_valid_user(person.value));
});

// Injects an account number of length 15
rc::check([](const faker::rc::finance::AccountNumber<15> &acct) {
    RC_ASSERT(acct.value.size() == 15);
});
```
