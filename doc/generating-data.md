# Generating Data

Generating data is done with the faker library (`faker::faker` in CMake).  The library
is pretty well subdivided into different parts. A `faker.h` header is provided for
convenience to import everything.

Every method accepts an optional `size_t` as an "index." This index is the basis
for the data generation. Everything is deterministic based on the index. If the
index isn't provided, then a Mersenne Twister random number generator will be
used instead (this provides a "random" default behavior).

By having everything be based off the index, it allows users to control how the
indices are generated (e.g. separate per thread, using a different algorithm, etc.).

## Modules

There are several modules provided which have categorized data which is generated.

### ASCII

This module is for generating ASCII text. It can do numeric, alphabetic, alphanumeric,
and hex text.

### Finance

This module is for generating fake financial data (credit cards, bank accounts,
etc).

### Malicious

This module is for generating malicious inputs (bad encodings, SQL injection, etc).
This is for testing proper best practices for handling malicious input.

### Person

This module is for generating data related to people (e.g. names).

### World

This module is for generating world-related data which doesn't fit in the other
modules.
